package com.github.xabgesagtx.mensa.exports;

import com.github.xabgesagtx.mensa.config.MensaApiProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ExportsService {

	private final static String EXPORTS_BY_ID_PATH_TEMPLATE = "/rest/exports/{id}";

	private final ExportsApiClient client;
	private final MensaApiProperties properties;

	List<ExportDTO> getExports() {
		return client.getExports()
				.stream()
				.map(this::convert)
				.collect(Collectors.toList());
	}

	private ExportDTO convert(ExportApiDTO export) {
		String url = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(EXPORTS_BY_ID_PATH_TEMPLATE)
				.buildAndExpand(export.getDateTimeId())
				.toUriString();
		return new ExportDTO(url, export.getDateTime(), export.getSize());
	}
}
