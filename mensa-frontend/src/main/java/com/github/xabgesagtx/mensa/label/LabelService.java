package com.github.xabgesagtx.mensa.label;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LabelService {

	private final LabelApiClient client;
	private final LabelProperties properties;

	public LabelWebDTO getLabel(String name) {
		LabelWebDTO.LabelWebDTOBuilder builder = LabelWebDTO.builder().name(name);
		findLabel(name).ifPresent(label -> builder.imageUrl(label.getImageUrl()));
		return builder.build();
	}

	private Optional<LabelApiDTO> findLabel(String searchText) {
		String simplifiedSearchText = simplify(searchText);
		return client.findAll().stream().filter(label -> isLabel(label,simplifiedSearchText)).findFirst();
	}

	private boolean isLabel(LabelApiDTO label, String simplifiedSearchText) {
		String simplifiedLabelName = simplify(label.getName());
		return simplifiedLabelName.equals(simplifiedSearchText) || StringUtils.startsWith(simplifiedLabelName, simplifiedSearchText) || StringUtils.startsWith(simplifiedSearchText, simplifiedLabelName);
	}

	private String simplify(String text) {
		return removeStopWords(StringUtils.defaultString(text).toLowerCase(Locale.GERMANY)).replaceAll("\\s", StringUtils.EMPTY);
	}

	private String removeStopWords(String text) {
		String cleanText = text;
		for (String stopword : properties.getStopwords()) {
			cleanText = StringUtils.removeStart(cleanText, stopword + " ");
		}
		return cleanText;
	}


	public List<String> getLabelsFromDishes() {
		return client.getLabelNamesFromDishes();
	}

}
