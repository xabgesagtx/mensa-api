package com.github.xabgesagtx.mensa.label;

import com.github.xabgesagtx.mensa.common.MensaApiException;
import com.github.xabgesagtx.mensa.config.MensaApiProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class LabelApiClient {

	private static final String LABELS_PATH = "/rest/label";
	private static final String LABEL_NAMES_FROM_DISHES_PATH = "/rest/dish/labels";

	private final RestTemplate restTemplate;
	private final MensaApiProperties properties;

	@Cacheable("labels")
	public List<LabelApiDTO> findAll() {
		log.info("Retrieving labels");
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(LABELS_PATH)
				.toUriString();
		try {
			return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<LabelApiDTO>>() {}).getBody();
		} catch (RestClientException e) {
			throw new MensaApiException("Failed to retrieve labels", e);
		}
	}

	List<String> getLabelNamesFromDishes() {
		log.info("Retrieving label names from dishes");
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(LABEL_NAMES_FROM_DISHES_PATH)
				.toUriString();
		try {
			return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<String>>() {}).getBody();
		} catch (RestClientException e) {
			throw new MensaApiException("Failed to retrieve label names from dishes", e);
		}
	}

}
