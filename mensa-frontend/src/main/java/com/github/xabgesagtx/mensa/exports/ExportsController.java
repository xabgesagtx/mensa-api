package com.github.xabgesagtx.mensa.exports;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("exports")
@RequiredArgsConstructor
public class ExportsController {

	private final ExportsService exportsService;

	@RequestMapping("")
	public ModelAndView all(ModelAndView modelAndView) {
		modelAndView.setViewName("exports");
		modelAndView.addObject("exports", exportsService.getExports());
		return modelAndView;
	}

}
