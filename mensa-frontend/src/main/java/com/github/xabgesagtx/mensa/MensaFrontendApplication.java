package com.github.xabgesagtx.mensa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * Main class for the http application
 */
@SpringBootApplication
@EnableCaching
public class MensaFrontendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MensaFrontendApplication.class, args);
	}

}
