package com.github.xabgesagtx.mensa.dish;

import com.github.xabgesagtx.mensa.common.MensaApiException;
import com.github.xabgesagtx.mensa.config.MensaApiProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
class DishApiClient {

	private static final String DISHES_PATH = "/rest/dish";
	private static final String DISHES_SEARCH_PATH = "/rest/dish/search";

	private final RestTemplate restTemplate;
	private final MensaApiProperties properties;

	List<DishApiDTO> find(String mensaId, LocalDate date) {
		log.info("Retrieving dishes for mensa {} and day {}", mensaId, date);
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(DISHES_PATH)
				.queryParam("mensaId", mensaId)
				.queryParam("date", date)
				.toUriString();
		try {
			return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<DishApiDTO>>() {}).getBody();
		} catch (
				RestClientException e) {
			throw new MensaApiException("Failed to retrieve dishes", e);
		}
	}

	PageApiDTO search(SearchRequest request) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(DISHES_SEARCH_PATH);
		if (StringUtils.isNotBlank(request.getCategory())) {
			builder.queryParam("category", request.getCategory());
		}
		if (StringUtils.isNotBlank(request.getDescription())) {
			builder.queryParam("description", request.getDescription());
		}
		if (StringUtils.isNotBlank(request.getLabels())) {
			builder.queryParam("labels", request.getLabels());
		}
		if (StringUtils.isNotBlank(request.getLabels())) {
			builder.queryParam("labels", request.getLabels());
		}
		if (StringUtils.isNotBlank(request.getMensaId())) {
			builder.queryParam("mensaId", request.getMensaId());
		}
		if (request.getFrom() != null) {
			builder.queryParam("from", request.getFrom());
		}
		if (request.getTo() != null) {
			builder.queryParam("to", request.getTo());
		}
		if (request.getPage() != null) {
			builder.queryParam("page", request.getPage());
		}

		String uri = builder.toUriString();
		log.info("Searching for: {}", uri);
		try {
			return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<PageApiDTO>() {}).getBody();
		} catch (
				RestClientException e) {
			throw new MensaApiException("Failed to retrieve dishes", e);
		}
	}

}
