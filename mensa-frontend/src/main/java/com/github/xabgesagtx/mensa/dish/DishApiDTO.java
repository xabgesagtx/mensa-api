package com.github.xabgesagtx.mensa.dish;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
class DishApiDTO {

	private String id;
	private LocalDate date;
	private String mensaId;
	private String category;
	private String description;
	private List<String> labels;
	private List<BigDecimal> prices;
	private List<String> allergens;

}
