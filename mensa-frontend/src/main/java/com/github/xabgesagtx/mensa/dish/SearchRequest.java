package com.github.xabgesagtx.mensa.dish;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class SearchRequest {
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate from;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate to;
	private String labels;
	private String category;
	private String mensaId;
	private String description;
	private Integer page;
}
