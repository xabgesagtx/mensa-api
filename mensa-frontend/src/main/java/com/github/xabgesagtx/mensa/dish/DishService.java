package com.github.xabgesagtx.mensa.dish;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DishService {

	private final DishWebDTOFactory factory;
	private final DishApiClient client;

	public List<DishWebDTO> find(String mensaId, LocalDate date) {
		return client.find(mensaId, date).stream()
				.map(factory::create)
				.collect(Collectors.toList());
	}

	public PageWebDTO findAll(SearchRequest request) {
		PageApiDTO page = client.search(request);
		List<DishWebDTO> dishes = page.getContent().stream()
				.map(factory::create)
				.collect(Collectors.toList());
		return PageWebDTO.builder()
				.content(dishes)
				.pageNumber(page.getPageNumber())
				.totalPages(page.getTotalPages())
				.totalElements(page.getTotalElements())
				.pageSize(10)
				.build();
	}
}
