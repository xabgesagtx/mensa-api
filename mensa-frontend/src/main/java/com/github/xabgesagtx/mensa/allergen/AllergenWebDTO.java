package com.github.xabgesagtx.mensa.allergen;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(staticName = "of")
public class AllergenWebDTO {
	String number;
	String name;
}
