package com.github.xabgesagtx.mensa.allergen;

import com.github.xabgesagtx.mensa.common.MensaApiException;
import com.github.xabgesagtx.mensa.config.MensaApiProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class AllergenApiClient {

	private static final String ALLERGENS_PATH = "/rest/allergen";

	private final RestTemplate restTemplate;
	private final MensaApiProperties properties;

	@Cacheable("allergens")
	public List<AllergenApiDTO> findAll() {
		log.info("Retrieving allergens");
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(ALLERGENS_PATH)
				.toUriString();
		try {
			return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<AllergenApiDTO>>() {}).getBody();
		} catch (
				RestClientException e) {
			throw new MensaApiException("Failed to retrieve allergens", e);
		}
	}
}
