package com.github.xabgesagtx.mensa.mensa;

import lombok.Data;

@Data
class MensaApiDTO {

	private String id;
	private String name;
	private String mainUrl;
	private String address;
	private String zipcode;
	private String city;

}
