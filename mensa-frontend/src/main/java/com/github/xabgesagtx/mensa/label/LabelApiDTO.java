package com.github.xabgesagtx.mensa.label;

import lombok.Value;

@Value
class LabelApiDTO {
	String id;
	String name;
	String imageUrl;
}
