package com.github.xabgesagtx.mensa.label;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "labels")
@Getter
@Setter
public class LabelProperties {
    private List<String> stopwords;
}
