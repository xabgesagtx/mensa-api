package com.github.xabgesagtx.mensa.dish;

import com.github.xabgesagtx.mensa.allergen.AllergenService;
import com.github.xabgesagtx.mensa.allergen.AllergenWebDTO;
import com.github.xabgesagtx.mensa.label.LabelService;
import com.github.xabgesagtx.mensa.label.LabelWebDTO;
import com.github.xabgesagtx.mensa.mensa.MensaService;
import com.github.xabgesagtx.mensa.mensa.MensaWebDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
class DishWebDTOFactory {

	private final LabelService labelService;
	private final AllergenService allergenService;
	private final MensaService mensaService;

	DishWebDTO create(DishApiDTO dish) {
		List<LabelWebDTO> labels = dish.getLabels().stream().map(labelService::getLabel).collect(Collectors.toList());
		List<AllergenWebDTO> allergens = dish.getAllergens().stream().map(allergenService::getAllergen).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
		MensaWebDTO mensa = mensaService.findById(dish.getMensaId()).orElse(null);
		return DishWebDTO.of(dish.getId(), dish.getDate(), dish.getDescription(), dish.getCategory(), allergens, labels, dish.getPrices(), mensa);
	}

}
