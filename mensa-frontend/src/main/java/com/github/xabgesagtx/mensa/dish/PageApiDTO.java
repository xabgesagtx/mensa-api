package com.github.xabgesagtx.mensa.dish;

import lombok.Data;

import java.util.List;

@Data
class PageApiDTO {
	private List<DishApiDTO> content;
	private int pageNumber;
	private int totalPages;
	private int pageSize;
	private long totalElements;
}
