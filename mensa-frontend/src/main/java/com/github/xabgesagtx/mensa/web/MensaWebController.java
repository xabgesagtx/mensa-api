package com.github.xabgesagtx.mensa.web;

import com.github.xabgesagtx.mensa.categories.CategoryService;
import com.github.xabgesagtx.mensa.common.ResourceNotFoundException;
import com.github.xabgesagtx.mensa.dish.DishService;
import com.github.xabgesagtx.mensa.dish.DishWebDTO;
import com.github.xabgesagtx.mensa.dish.PageWebDTO;
import com.github.xabgesagtx.mensa.dish.SearchRequest;
import com.github.xabgesagtx.mensa.label.LabelService;
import com.github.xabgesagtx.mensa.mensa.MensaService;
import com.github.xabgesagtx.mensa.mensa.MensaWebDTO;
import com.github.xabgesagtx.mensa.time.TimeUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Controller for mensa overview and detail pages that display the dishes
 */
@Controller
@RequestMapping("")
@RequiredArgsConstructor
public class MensaWebController {

    private final MensaService mensaService;
    private final DishService dishService;
    private final LabelService labelService;
    private final CategoryService categoryService;
    private final TimeUtils timeUtils;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView allMensas(ModelAndView modelAndView) {
        modelAndView.addObject("mensas", mensaService.findAll());
        modelAndView.setViewName("mensas");
        return modelAndView;
    }

    @RequestMapping(value = "mensa/{id}", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView nextOpeningDay(@PathVariable("id") String id, ModelAndView modelAndView) {
        MensaWebDTO mensa = findMensaWithNullCheck(id);
        return renderMenu(mensa, timeUtils.nextOpeningDay(mensa.getId()), modelAndView);
    }

    private MensaWebDTO findMensaWithNullCheck(String id) {
        return mensaService.findById(id).orElseThrow(() -> new ResourceNotFoundException("No mensa with id " + id));
    }

    @RequestMapping(value = "mensa/{id}/{date}", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView day(@PathVariable("id") String id, @PathVariable("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date, ModelAndView modelAndView) {
        MensaWebDTO mensa = findMensaWithNullCheck(id);
        return renderMenu(mensa, date, modelAndView);
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    public ModelAndView findDishes(SearchRequest request, ModelAndView modelAndView) {
		PageWebDTO page = dishService.findAll(request);
		modelAndView.addObject("page", page);
		modelAndView.addObject("dishes", page.getContent());
		modelAndView.addObject("labels", labelService.getLabelsFromDishes());
		modelAndView.addObject("categories", categoryService.getCategoriesFromDishes());
		modelAndView.addObject("mensas", mensaService.findAll());
        modelAndView.setViewName("search");
        return modelAndView;
    }

    private ModelAndView renderMenu(MensaWebDTO mensa, LocalDate date, ModelAndView modelAndView) {
        modelAndView.addObject("mensa", mensa);
        modelAndView.addObject("date", DateTimeFormatter.ofPattern("EEEE, dd.MM.yyyy", Locale.ENGLISH).format(date));
        List<DishWebDTO> dishes = dishService.find(mensa.getId(), date);
		List<String> categories = dishes.stream().map(DishWebDTO::getCategory).distinct().collect(Collectors.toList());
		Map<String,List<DishWebDTO>> dishesMap = dishes.stream().collect(Collectors.groupingBy(DishWebDTO::getCategory, Collectors.toList()));
		modelAndView.addObject("categories", categories);
		modelAndView.addObject("dishesMap", dishesMap);
        modelAndView.addObject("next", timeUtils.nextOpeningDay(date.plusDays(1), mensa.getId()));
        modelAndView.addObject("previous", timeUtils.lastOpeningDay(date.minusDays(1), mensa.getId()));
        modelAndView.setViewName("menu");
        return modelAndView;
    }

}
