package com.github.xabgesagtx.mensa.exports;

import com.github.xabgesagtx.mensa.common.MensaApiException;
import com.github.xabgesagtx.mensa.config.MensaApiProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
@RequiredArgsConstructor
class ExportsApiClient {

	private static final String EXPORTS_PATH = "/rest/exports";

	private final RestTemplate restTemplate;
	private final MensaApiProperties properties;

	List<ExportApiDTO> getExports() {
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(EXPORTS_PATH)
				.toUriString();
		try {
			return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<ExportApiDTO>>() {}).getBody();
		} catch (RestClientException e) {
			throw new MensaApiException("Failed to get list of exports", e);
		}
	}

}
