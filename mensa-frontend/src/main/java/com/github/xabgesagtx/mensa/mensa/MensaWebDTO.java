package com.github.xabgesagtx.mensa.mensa;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class MensaWebDTO {

	String id;
	String name;
	String mainUrl;
	String address;
	String zipcode;
	String city;
}
