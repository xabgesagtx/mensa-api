package com.github.xabgesagtx.mensa.mensa;

import com.github.xabgesagtx.mensa.common.MensaApiException;
import com.github.xabgesagtx.mensa.config.MensaApiProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class MensaApiClient {

	private static final String MENSAS_PATH = "/rest/mensa";

	private final RestTemplate restTemplate;
	private final MensaApiProperties properties;

	@Cacheable("mensas")
	public List<MensaApiDTO> findAll() {
		log.info("Retrieving mensas");
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(MENSAS_PATH)
				.toUriString();
		try {
			return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<MensaApiDTO>>() {}).getBody();
		} catch (RestClientException e) {
			throw new MensaApiException("Failed to retrieve mensas", e);
		}
	}
}
