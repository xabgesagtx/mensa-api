package com.github.xabgesagtx.mensa.categories;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {

	private final CategoryApiClient client;

	public List<String> getCategoriesFromDishes() {
		return client.getCategoriesFromDishes();
	}
}
