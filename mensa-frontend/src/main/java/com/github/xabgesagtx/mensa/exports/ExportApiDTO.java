package com.github.xabgesagtx.mensa.exports;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ExportApiDTO {
	private String dateTimeId;
	private LocalDateTime dateTime;
	private String size;
}
