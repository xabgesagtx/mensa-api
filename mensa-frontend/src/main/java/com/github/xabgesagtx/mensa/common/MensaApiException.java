package com.github.xabgesagtx.mensa.common;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_GATEWAY)
public class MensaApiException extends RuntimeException {

	public MensaApiException(String message, Throwable cause) {
		super(message, cause);
	}

}
