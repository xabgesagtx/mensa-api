package com.github.xabgesagtx.mensa.mensa;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MensaService {

	private final MensaApiClient client;

	public List<MensaWebDTO> findAll() {
		return client.findAll().stream().map(this::toWebDto).collect(Collectors.toList());
	}

	public Optional<MensaWebDTO> findById(String id) {
		return client.findAll().stream()
				.filter(mensa -> StringUtils.equals(mensa.getId(), id))
				.map(this::toWebDto)
				.findFirst();
	}

	private MensaWebDTO toWebDto(MensaApiDTO mensa) {
		return MensaWebDTO.builder()
				.id(mensa.getId())
				.name(mensa.getName())
				.address(mensa.getAddress())
				.city(mensa.getCity())
				.zipcode(mensa.getZipcode())
				.mainUrl(mensa.getMainUrl())
				.build();
	}
}
