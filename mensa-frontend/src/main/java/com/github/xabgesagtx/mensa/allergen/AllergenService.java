package com.github.xabgesagtx.mensa.allergen;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AllergenService {

	private final AllergenApiClient client;

	public Optional<AllergenWebDTO> getAllergen(String number) {
		return client.findAll().stream()
				.filter(allergen -> allergen.getNumber().equals(number))
				.map(this::toWebDto)
				.findFirst();
	}

	private AllergenWebDTO toWebDto(AllergenApiDTO allergen) {
		return AllergenWebDTO.of(allergen.getNumber(), allergen.getName());
	}

}
