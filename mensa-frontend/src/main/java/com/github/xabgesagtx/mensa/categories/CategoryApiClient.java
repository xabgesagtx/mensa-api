package com.github.xabgesagtx.mensa.categories;

import com.github.xabgesagtx.mensa.common.MensaApiException;
import com.github.xabgesagtx.mensa.config.MensaApiProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
class CategoryApiClient {

	private static final String CATEGORIES_FROM_DISHES_PATH = "/rest/dish/categories";

	private final RestTemplate restTemplate;
	private final MensaApiProperties properties;

	List<String> getCategoriesFromDishes() {
		log.info("Retrieving categories from dishes");
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(CATEGORIES_FROM_DISHES_PATH)
				.toUriString();
		try {
			return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<String>>() {}).getBody();
		} catch (
				RestClientException e) {
			throw new MensaApiException("Failed to retrieve labels", e);
		}
	}
}
