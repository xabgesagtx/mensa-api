package com.github.xabgesagtx.mensa.exports;

import lombok.Value;
import org.apache.commons.lang3.builder.CompareToBuilder;

import java.time.LocalDateTime;

@Value
public class ExportDTO implements Comparable<ExportDTO> {
	String url;
	LocalDateTime dateTime;
	String size;

	@Override
	public int compareTo(ExportDTO o) {
		return new CompareToBuilder().append(o.dateTime,dateTime).toComparison();
	}
}
