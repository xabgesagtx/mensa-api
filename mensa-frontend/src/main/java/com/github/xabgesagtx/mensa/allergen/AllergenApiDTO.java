package com.github.xabgesagtx.mensa.allergen;

import lombok.Data;

@Data
class AllergenApiDTO {

	private String number;
	private String name;

}
