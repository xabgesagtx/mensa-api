package com.github.xabgesagtx.mensa.dish;

import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Value
@Builder
public class PageWebDTO {

	@Value
	static class PageItem {
		int pageNumber;
		boolean currentPage;
		public String getLabel() {
			return String.valueOf(pageNumber + 1);
		}
	}

	List<DishWebDTO> content;
	int pageNumber;
	int totalPages;
	int pageSize;
	long totalElements;

	public List<PageItem> getPageItems() {
		return IntStream.range(0, totalPages).mapToObj(page -> new PageItem(page, page == pageNumber)).collect(Collectors.toList());
	}

	public boolean isLastPage() {
		return pageNumber == totalPages -1;
	}

	public boolean isFirstPage() {
		return pageNumber == 0;
	}

	public String getSummary() {
		if (totalElements == 0) {
			return "No entries";
		} else {
			int start = pageNumber * pageSize + 1;
			long end = Math.min((pageNumber + 1) * pageSize, totalElements);
			return String.format("%s-%s of %s entries", start, end, totalElements);
		}
	}
}
