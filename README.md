
# Mensa API

[![pipeline status](https://gitlab.com/xabgesagtx/mensa-api/badges/master/pipeline.svg)](https://gitlab.com/xabgesagtx/mensa-api/commits/master)

This project aims to provide the means to create an API for all data about mensas in Hamburg.

[Live example exists online](https://mensa.canceledsystems.com)

![Mensa API HTML View](screenshots/screenshot.png "Mensa API HTML view")

## Features

* indexing of all menus for the mensas in Hamburg
* REST-API with Swagger documentation and Swagger UI view
* responsive HTML view of menus
* GraphQL-API with GraphiQL view 
* CSV export of mensa data to S3 for usage in other languages and tools (e.g. R)
* access of menus via Telegram bot

## Components

There are the following components

* *mensa-time* provides the shared code for time related issues
* *mensa-backend* indexes mensa data and provides access to it via an API
* *mensa-frontend* provides HTML view to the mensa data and exports
* *mensa-telegram* provides a Telegram bot for accessing the mensa data

Except of the the mensa-time component are all components Spring Boot applications.

To compile the project run `./gradlew build`

Afterwards you can execute the compiled jar files right away.

## Configuration

For configuration you can provide an application.yml file next to each jar file.

### Backend

For the backend to run you need to set the credentials and location of the S3 compliant store where the final zip file with exported data should be copied to
```yaml
export:
  s3:
   key: ACCESS_KEY
   secret: ACCESS_SECRET
   region: REGION
   endpoint: S3_ENDPOINT
   bucket: NAME_OF_BUCKET_FOR_EXPORTS
```

The most important configuration would be the configuration of the MongoDB connection.

```yaml
spring:
  data:
    mongodb:
      database: DB_NAME
      host: HOSTNAME
      username: USERNAME
      password: PASSWORD
```

### Frontend

You might want to set the port where the http frontend should be running
```yaml
server:
  port: 8080
```

### Telegram

For telegram usage you need to set the name of your bot and its token
```yaml
telegram:
  botname: NAME
  token: TOKEN

```

The telegram bot persists which user has selected which mensa. Therefore it needs a mongo connection as well.

```yaml
spring:
  data:
    mongodb:
      database: DB_NAME
      host: HOSTNAME
      username: USERNAME
      password: PASSWORD
```

## Requirements

* MongoDB
* S3 compliant store
* Java 11
* telegram bot credentials

## Development

### Intellij Idea

If you use Intellij Idea in `Build, Execution, Deployment > Build Tools > Gradle` deactivate `Create separate module per source set`. Otherwise you would get problems with the query dsl classes.

### Docker 

To quickly get a mongodb up and running there is a docker compose file with a mongodb container. Just run the following command before starting the application locally:

```
docker-compose up -d
```

Of course you need `docker` and `docker-compose` installed for this.
