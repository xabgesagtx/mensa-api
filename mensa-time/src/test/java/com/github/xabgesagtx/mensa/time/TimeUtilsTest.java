package com.github.xabgesagtx.mensa.time;

import org.junit.Test;

import java.time.LocalDate;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TimeUtilsTest {

    private static final String MENSA_ID_CLOSED_ON_SATURDAY = "100";
    private static final String MENSA_ID_OPEN_ON_SATURDAY = "101";


    private TimeUtils timeUtils = new TimeUtils(Collections.singletonList(MENSA_ID_OPEN_ON_SATURDAY));

    @Test
    public void nextOpeningDayClosedOnSaturday() {
        assertThat(timeUtils.nextOpeningDay(LocalDate.of(2017, 6, 26), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 26)));
        assertThat(timeUtils.nextOpeningDay(LocalDate.of(2017, 6, 25), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 26)));
        assertThat(timeUtils.nextOpeningDay(LocalDate.of(2017, 6, 24), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 26)));
        assertThat(timeUtils.nextOpeningDay(LocalDate.of(2017, 6, 23), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 23)));
        assertThat(timeUtils.nextOpeningDay(LocalDate.of(2017, 5, 1), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(LocalDate.of(2017, 5, 2)));
    }

    @Test
    public void nextOpeningDayOpenOnSaturday() {
        assertThat(timeUtils.nextOpeningDay(LocalDate.of(2017, 6, 26), MENSA_ID_OPEN_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 26)));
        assertThat(timeUtils.nextOpeningDay(LocalDate.of(2017, 6, 25), MENSA_ID_OPEN_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 26)));
        assertThat(timeUtils.nextOpeningDay(LocalDate.of(2017, 6, 24), MENSA_ID_OPEN_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 24)));
        assertThat(timeUtils.nextOpeningDay(LocalDate.of(2017, 6, 23), MENSA_ID_OPEN_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 23)));
        assertThat(timeUtils.nextOpeningDay(LocalDate.of(2017, 5, 1), MENSA_ID_OPEN_ON_SATURDAY), equalTo(LocalDate.of(2017, 5, 2)));
    }

    @Test
    public void lastOpeningDayClosedOnSaturday() {
        assertThat(timeUtils.lastOpeningDay(LocalDate.of(2017, 6, 26), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 26)));
        assertThat(timeUtils.lastOpeningDay(LocalDate.of(2017, 6, 25), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 23)));
        assertThat(timeUtils.lastOpeningDay(LocalDate.of(2017, 6, 24), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 23)));
        assertThat(timeUtils.lastOpeningDay(LocalDate.of(2017, 6, 23), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 23)));
        assertThat(timeUtils.lastOpeningDay(LocalDate.of(2017, 5, 1), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(LocalDate.of(2017, 4, 28)));
    }

    @Test
    public void lastOpeningDayOpenOnSaturday() {
        assertThat(timeUtils.lastOpeningDay(LocalDate.of(2017, 6, 26), MENSA_ID_OPEN_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 26)));
        assertThat(timeUtils.lastOpeningDay(LocalDate.of(2017, 6, 25), MENSA_ID_OPEN_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 24)));
        assertThat(timeUtils.lastOpeningDay(LocalDate.of(2017, 6, 24), MENSA_ID_OPEN_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 24)));
        assertThat(timeUtils.lastOpeningDay(LocalDate.of(2017, 6, 23), MENSA_ID_OPEN_ON_SATURDAY), equalTo(LocalDate.of(2017, 6, 23)));
        assertThat(timeUtils.lastOpeningDay(LocalDate.of(2017, 5, 1), MENSA_ID_OPEN_ON_SATURDAY), equalTo(LocalDate.of(2017, 4, 29)));
    }

    @Test
    public void isOpeningDayClosedOnSaturday() {
        assertThat(timeUtils.isOpeningDay(LocalDate.of(2017, 6, 26), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(true));
        assertThat(timeUtils.isOpeningDay(LocalDate.of(2017, 6, 25), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(false));
        assertThat(timeUtils.isOpeningDay(LocalDate.of(2017, 6, 24), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(false));
        assertThat(timeUtils.isOpeningDay(LocalDate.of(2017, 6, 23), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(true));
        assertThat(timeUtils.isOpeningDay(LocalDate.of(2017, 5, 1), MENSA_ID_CLOSED_ON_SATURDAY), equalTo(false));
    }

    @Test
    public void isOpeningDayOpenOnSaturday() {
        assertThat(timeUtils.isOpeningDay(LocalDate.of(2017, 6, 26), MENSA_ID_OPEN_ON_SATURDAY), equalTo(true));
        assertThat(timeUtils.isOpeningDay(LocalDate.of(2017, 6, 25), MENSA_ID_OPEN_ON_SATURDAY), equalTo(false));
        assertThat(timeUtils.isOpeningDay(LocalDate.of(2017, 6, 24), MENSA_ID_OPEN_ON_SATURDAY), equalTo(true));
        assertThat(timeUtils.isOpeningDay(LocalDate.of(2017, 6, 23), MENSA_ID_OPEN_ON_SATURDAY), equalTo(true));
        assertThat(timeUtils.isOpeningDay(LocalDate.of(2017, 5, 1), MENSA_ID_OPEN_ON_SATURDAY), equalTo(false));
    }
    
}