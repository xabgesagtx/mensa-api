package com.github.xabgesagtx.mensa.time;

import de.jollyday.HolidayCalendar;
import de.jollyday.HolidayManager;
import de.jollyday.ManagerParameters;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * Utility class to handle time specific question mostly regarding the topic when the mensa will be or was open
 */
public class TimeUtils {


    private static final HolidayManager HOLIDAY_MANAGER = HolidayManager.getInstance(ManagerParameters.create(HolidayCalendar.GERMANY));

    private List<String> mensaIdsOpenOnSaturday = Collections.singletonList("350");

    public TimeUtils() {

    }

    public TimeUtils(List<String> mensaIdsOpenOnSaturday) {
        this.mensaIdsOpenOnSaturday = mensaIdsOpenOnSaturday;
    }

    /**
     * Returns next opening relative to now. If the mensa is open on that day, the date is returned
     * @param mensaId to check
     * @return next opening relative to passed date
     */
    public LocalDate nextOpeningDay(String mensaId) {
        return nextOpeningDay(LocalDate.now(), mensaId);
    }

    /**
     * Returns next opening relative to passed date. If the mensa is open on that day, the date is returned
     * @param date to find next opening day for
     * @param mensaId to check
     * @return next opening relative to passed date
     */
    public LocalDate nextOpeningDay(LocalDate date, String mensaId) {
        LocalDate result = date;
        while (!isOpeningDay(result, mensaId)) {
            result = result.plusDays(1);
        }
        return result;
    }

    /**
     * Returns last opening relative to passed date. If the mensa is open on that day, the date is returned
     * @param date to find last opening day for
     * @param mensaId to check
     * @return last opening relative to passed date
     */
    public LocalDate lastOpeningDay(LocalDate date, String mensaId) {
        LocalDate result = date;
        while (!isOpeningDay(result, mensaId)) {
            result = result.minusDays(1);
        }
        return result;
    }

    /**
     * Check if date is opening day for a specific mensa
     * @param date to check
     * @param mensaId to check
     * @return true if is opening day, false otherwise
     */
    public boolean isOpeningDay(LocalDate date, String mensaId) {
        DayOfWeek firstDayClosed = isMensaOpenOnSaturday(mensaId) ? DayOfWeek.SUNDAY : DayOfWeek.SATURDAY;
        return date.getDayOfWeek().compareTo(firstDayClosed) < 0 && !HOLIDAY_MANAGER.isHoliday(date, "hh");
    }

    public boolean isMensaOpenOnSaturday(String mensaId) {
        return mensaIdsOpenOnSaturday.contains(mensaId);
    }
}
