package com.github.xabgesagtx.mensa.scraping;

import com.fasterxml.jackson.core.type.TypeReference;
import com.github.xabgesagtx.mensa.scraping.model.MenuUrls;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class MenuUrlScraperTest {

	@InjectMocks
	private MenuUrlScraper urlScraper;

	@Test
	public void testGetMenuUrlsFromString() throws IOException {
		String htmlString = TestUtils.readStringFromClasspath("/scraping/mensa.html");
		MenuUrls expected = TestUtils.readObjectFromClasspath("/scraping/mensa.json", new TypeReference<MenuUrls>(){});
		Optional<MenuUrls> actual = urlScraper.scrapeFromString(htmlString, "http://example.com/");
		assertTrue(actual.isPresent());
		assertThat(actual.get(), equalTo(expected));
	}

}
