package com.github.xabgesagtx.mensa.scraping;

import com.fasterxml.jackson.core.type.TypeReference;
import com.github.xabgesagtx.mensa.model.Allergen;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class AllergenScraperTest {

    @InjectMocks
    private AllergenScraper scraper;

    @Test
    public void testScrapeFromString() throws Exception {
        String htmlString = TestUtils.readStringFromClasspath("/scraping/allergenAndLabel.html");
        List<Allergen> expected = TestUtils.readObjectFromClasspath("/scraping/allergens.json", new TypeReference<List<Allergen>>(){});
        List<Allergen> was = scraper.scrapeFromString(htmlString, "http://example.com/");
        assertThat(was, equalTo(expected));
    }

}