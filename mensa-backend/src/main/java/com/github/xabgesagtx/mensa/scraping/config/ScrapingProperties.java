package com.github.xabgesagtx.mensa.scraping.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Main config file to set urls to scraping
 */
@Component
@ConfigurationProperties(prefix = "scraping")
@Setter
@Getter
public class ScrapingProperties {

    String mensasUrl;
    String allergenAndCategoryUrl;
    String nominatinUrl;
    Map<String,GeoPointConfig> coordinatesForMensaId = new HashMap<>();

}
