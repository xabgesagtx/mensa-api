package com.github.xabgesagtx.mensa.export;

import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public final class ExportConstants {
	public static final String EXPORT_DATE_FORMAT = "yyyyMMddHHmm";
	public static final DateTimeFormatter EXPORT_DATE_FORMATTER = DateTimeFormatter.ofPattern(EXPORT_DATE_FORMAT);
}
