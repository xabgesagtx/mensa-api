package com.github.xabgesagtx.mensa.scraping.config;

import com.github.xabgesagtx.mensa.scraping.model.MensaDetails;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@ConfigurationProperties("scraping.mensa")
@Component
public class MensaDefaultProperties {

	@Getter
	@Setter
	private Map<String, MensaDetails> details;

}
