package com.github.xabgesagtx.mensa.api.rest.model;

import com.github.xabgesagtx.mensa.model.Dish;
import lombok.Value;

import java.util.List;

@Value
public class PageDTO {
	List<Dish> content;
	int pageNumber;
	int totalPages;
	int pageSize;
	long totalElements;
}
