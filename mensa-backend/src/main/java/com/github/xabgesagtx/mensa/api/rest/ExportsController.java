package com.github.xabgesagtx.mensa.api.rest;

import com.github.xabgesagtx.mensa.export.ExportConstants;
import com.github.xabgesagtx.mensa.export.ExportRepository;
import com.github.xabgesagtx.mensa.export.model.ExportData;
import com.github.xabgesagtx.mensa.export.model.ExportSummary;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("rest/exports")
@Api(tags = "exports")
@Slf4j
@RequiredArgsConstructor
public class ExportsController {

	private final ExportRepository exportRepository;


	@GetMapping("")
	public List<ExportSummary> all() {
		return exportRepository.findAll();
	}

	@GetMapping("{dateTimeId}")
	public ResponseEntity<InputStreamResource> getExport(@PathVariable("dateTimeId") @DateTimeFormat(pattern = ExportConstants.EXPORT_DATE_FORMAT) LocalDateTime dateTime) {
		Optional<ExportData> exportFile = exportRepository.getExport(dateTime);
		return exportFile.map(this::toInputStreamResource).orElseGet(() -> ResponseEntity.notFound().build());
	}

	private ResponseEntity<InputStreamResource> toInputStreamResource(ExportData file) {
		InputStreamResource resource = new InputStreamResource(file.getInputStream());
		return ResponseEntity.ok()
				.contentLength(file.getSize())
				.contentType(MediaType.parseMediaType("application/zip"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getDateTimeId() + ".zip")
				.body(resource);
	}
}
