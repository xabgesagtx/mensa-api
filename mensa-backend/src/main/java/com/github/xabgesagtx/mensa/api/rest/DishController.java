package com.github.xabgesagtx.mensa.api.rest;

import com.github.xabgesagtx.mensa.api.rest.model.PageDTO;
import com.github.xabgesagtx.mensa.model.Dish;
import com.github.xabgesagtx.mensa.model.QDish;
import com.github.xabgesagtx.mensa.persistence.DishRepository;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Rest controller for dishes
 */
@RestController
@CrossOrigin
@RequestMapping("rest/dish")
@Api(tags = "dish")
@RequiredArgsConstructor
public class DishController {

    private final DishRepository repo;
    private final MongoTemplate template;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Dish> findDishes(@RequestParam(value = "mensaId") String mensaId, @RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        return repo.findByDateAndMensaIdOrderById(date,mensaId);
    }

    @GetMapping(value = "search", produces = MediaType.APPLICATION_JSON_VALUE)
    public PageDTO search(@QuerydslPredicate(root = Dish.class) Predicate predicate,
                          @PageableDefault(sort = {"date"}, direction = Sort.Direction.DESC) Pageable pageable) {
        Sort realSort = pageable.getSort().and(new Sort(Sort.Direction.ASC, "id"));
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), realSort);
        Page<Dish> result;
        if (predicate == null) {
            result = repo.findAll(pageRequest);
        } else {
            result = repo.findAll(predicate, pageRequest);
        }
        return new PageDTO(result.getContent(), result.getNumber(), result.getTotalPages(), pageable.getPageSize(), result.getTotalElements());
    }

    @GetMapping(value = "labels", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getLabelsFromDishes() {
        List<String> labels = new ArrayList<>();
        template.getCollection("dish").distinct("labels", String.class).iterator().forEachRemaining(labels::add);
        labels.sort(String.CASE_INSENSITIVE_ORDER);
        return labels;
    }

    @GetMapping(value = "categories", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getCategoriesFromDishes() {
        List<String> categories = new ArrayList<>();
        template.getCollection("dish").distinct(QDish.dish.category.getMetadata().getName(), String.class).iterator().forEachRemaining(categories::add);
        categories.sort(String.CASE_INSENSITIVE_ORDER);
        return categories;
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Dish byId(@PathVariable("id") String id) {
        return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No dish with id " + id));
    }
}
