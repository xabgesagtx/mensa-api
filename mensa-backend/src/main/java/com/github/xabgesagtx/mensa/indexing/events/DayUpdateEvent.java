package com.github.xabgesagtx.mensa.indexing.events;

import com.github.xabgesagtx.mensa.model.Dish;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.time.LocalDate;
import java.util.List;

/**
 * Event to hold all information for day to be updated, including the new dishes to be added
 */
@Getter
public class DayUpdateEvent extends ApplicationEvent {

	private String mensaId;
	private LocalDate day;
	private List<Dish> dishes;

	/**
	 * Create a new ApplicationEvent.
	 *
	 * @param source the object on which the event initially occurred (never {@code null})
	 */
	public DayUpdateEvent(String mensaId, LocalDate day, List<Dish> dishes, Object source) {
		super(source);
		this.mensaId = mensaId;
		this.day = day;
		this.dishes = dishes;
	}
}
