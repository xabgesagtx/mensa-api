package com.github.xabgesagtx.mensa.export.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ExportConfig {

	@Bean
	public TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor result = new ThreadPoolTaskExecutor();
		result.setMaxPoolSize(1);
		result.setCorePoolSize(1);
		return result;
	}

	@Bean
	public AmazonS3 s3Client(S3Properties properties) {
		BasicAWSCredentials credentials = new BasicAWSCredentials(properties.getKey(), properties.getSecret());
		AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(credentials);
		EndpointConfiguration endpointConfiguration = new EndpointConfiguration(properties.getEndpoint(), properties.getRegion());
		return AmazonS3ClientBuilder.standard()
				.withCredentials(credentialsProvider)
				.withEndpointConfiguration(endpointConfiguration)
				.withPathStyleAccessEnabled(true)
				.build();
	}
}
