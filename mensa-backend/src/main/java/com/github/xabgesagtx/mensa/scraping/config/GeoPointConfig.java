package com.github.xabgesagtx.mensa.scraping.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeoPointConfig {

	private Double longitude;
	private Double latitude;

}
