package com.github.xabgesagtx.mensa.indexing;

import com.github.xabgesagtx.mensa.indexing.events.DayUpdateEvent;
import com.github.xabgesagtx.mensa.indexing.events.MenuUpdateEvent;
import com.github.xabgesagtx.mensa.model.Dish;
import com.github.xabgesagtx.mensa.model.Mensa;
import com.github.xabgesagtx.mensa.scraping.MenuWeekScraper;
import com.github.xabgesagtx.mensa.scraping.SingleDayScraper;
import com.github.xabgesagtx.mensa.time.TimeUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Scrapes as many dishes as possible for a single mensa and trigger updates for each day with scraped dishes available
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class MensaMenuUpdater implements ApplicationListener<MenuUpdateEvent> {
	
	private final MenuWeekScraper scraper;
	private final SingleDayScraper singleDayScraper;
	private final TimeUtils timeUtils;
	private final ApplicationEventPublisher publisher;

	@Override
	public void onApplicationEvent(MenuUpdateEvent event) {
		Mensa mensa = event.getMensa();
		log.info("Starting indexing of mensa {} ({})", mensa.getName(), mensa.getId());
		List<Dish> dishes = scraper.scrape(mensa.getThisWeekUrl(), mensa.getId());
		dishes.addAll(scraper.scrape(mensa.getNextWeekUrl(), mensa.getId()));
		if (timeUtils.isMensaOpenOnSaturday(mensa.getId())) {
			dishes.addAll(scrapeSaturdayDishes(mensa));
		}
		log.info("Found total of {} dishes for mensa {} ({})", dishes.size(), mensa.getName(), mensa.getId());
		Map<LocalDate,List<Dish>> mapByDay = getMapByDay(dishes);
		mapByDay.forEach((key, value) -> startDayUpdate(mensa.getId(), key, value));
	}

	private List<Dish> scrapeSaturdayDishes(Mensa mensa) {
		DayOfWeek today = LocalDate.now().getDayOfWeek();
		List<Dish> saturdayDishes = new ArrayList<>();
		if (today.equals(DayOfWeek.FRIDAY) && StringUtils.isNotBlank(mensa.getTomorrowUrl())) {
			saturdayDishes = singleDayScraper.scrape(mensa.getTomorrowUrl(), mensa.getId());
		} else if (today.equals(DayOfWeek.SATURDAY) && StringUtils.isNotBlank(mensa.getTodayUrl())) {
			saturdayDishes = singleDayScraper.scrape(mensa.getTodayUrl(), mensa.getId());
		}
		List<Dish> result = saturdayDishes.stream().filter(dish -> dish.getDate().getDayOfWeek().equals(DayOfWeek.SATURDAY)).collect(Collectors.toList());
		log.info("Scraped {} dishes for saturday for mensa {} ({})", result.size(), mensa.getName(), mensa.getId());
		return result;
	}

	private void startDayUpdate(String mensaId, LocalDate date, List<Dish> dishes) {
		log.info("Sending indexing request for mensa {} at {} with {} dishes", mensaId, date.format(DateTimeFormatter.ISO_DATE), dishes.size());
		publisher.publishEvent(new DayUpdateEvent(mensaId, date, dishes, this));
	}

	private Map<LocalDate,List<Dish>> getMapByDay(List<Dish> dishes) {
		return dishes.stream().collect(Collectors.groupingBy(Dish::getDate));
	}
}
