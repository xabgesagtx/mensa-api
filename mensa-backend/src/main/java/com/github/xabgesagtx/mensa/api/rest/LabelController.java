package com.github.xabgesagtx.mensa.api.rest;

import com.github.xabgesagtx.mensa.model.Label;
import com.github.xabgesagtx.mensa.persistence.LabelRepository;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * Rest controller for labels
 */
@RestController
@CrossOrigin
@RequestMapping("rest/label")
@Api(tags = "label")
@RequiredArgsConstructor
public class LabelController {

    private final LabelRepository repo;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Label> all() {
        return repo.findAll();
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Label byId(@PathVariable("id") String id) {
        return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No label with id " + id));
    }

}
