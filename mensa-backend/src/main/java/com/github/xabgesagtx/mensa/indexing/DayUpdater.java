package com.github.xabgesagtx.mensa.indexing;


import com.github.xabgesagtx.mensa.indexing.events.DayUpdateEvent;
import com.github.xabgesagtx.mensa.model.Dish;
import com.github.xabgesagtx.mensa.persistence.DishRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Updater to replace old dishes for a day and replace them with new dishes
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class DayUpdater implements ApplicationListener<DayUpdateEvent> {
	
	private final DishRepository repo;

	@Override
	@Transactional
	public void onApplicationEvent(DayUpdateEvent event) {
		log.info("Starting indexing for day {} and mensa {}", event.getDay().format(DateTimeFormatter.ISO_DATE), event.getMensaId());
		List<Dish> oldDishes = repo.findByDateAndMensaIdOrderById(event.getDay(), event.getMensaId());
		repo.deleteAll(oldDishes);
		repo.saveAll(event.getDishes());
		log.info("Finished indexing for day {} and mensa {}. New: {}, Old: {}", event.getDay().format(DateTimeFormatter.ISO_DATE), event.getMensaId(), event.getDishes().size(), oldDishes.size());
	}
}
