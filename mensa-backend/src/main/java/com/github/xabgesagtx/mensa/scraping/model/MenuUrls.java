package com.github.xabgesagtx.mensa.scraping.model;

import lombok.*;

/**
 * Wrapper class to hold all relevant urls of a mensa
 */
@Getter
@AllArgsConstructor(staticName="of")
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class MenuUrls {
	
	private String today;
	private String tomorrow;
	private String thisWeek;
	private String nextWeek;

}
