package com.github.xabgesagtx.mensa.scraping.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MensaDetails {

	private String address;
	private String zipcode;
	private String city;

}
