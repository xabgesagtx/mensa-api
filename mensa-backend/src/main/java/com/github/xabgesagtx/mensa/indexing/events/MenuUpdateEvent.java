package com.github.xabgesagtx.mensa.indexing.events;

import com.github.xabgesagtx.mensa.model.Mensa;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;


/**
 * Event to signal that the menu of a mensa needs to be updated
 */
public class MenuUpdateEvent extends ApplicationEvent {

	@Getter
	private final Mensa mensa;

	/**
	 * Create a new MenuUpdateEvent.
	 *
	 * @param mensa mensa to indexing menu for
	 * @param source the object on which the event initially occurred (never {@code null})
	 */
	public MenuUpdateEvent(Mensa mensa, Object source) {
		super(source);
		this.mensa = mensa;
	}
}
