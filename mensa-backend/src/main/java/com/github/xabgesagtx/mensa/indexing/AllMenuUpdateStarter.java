package com.github.xabgesagtx.mensa.indexing;

import com.github.xabgesagtx.mensa.indexing.events.MenuUpdateEvent;
import com.github.xabgesagtx.mensa.model.Mensa;
import com.github.xabgesagtx.mensa.persistence.MensaRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Triggers indexing jobs for menus of all mensas in the database
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class AllMenuUpdateStarter {

	private final MensaRepository mensaRepo;
	private final ApplicationEventPublisher publisher;
	
	@Scheduled(cron = "${indexing.menu.cron}")
	public void startUpdates() {
		List<Mensa> mensas = mensaRepo.findAll();
		log.info("Starting indexing of menus for {} mensas", mensas.size());
		mensas.forEach(mensa -> publisher.publishEvent(new MenuUpdateEvent(mensa, this)));
	}

}
