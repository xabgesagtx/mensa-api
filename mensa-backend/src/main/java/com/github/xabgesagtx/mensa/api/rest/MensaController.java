package com.github.xabgesagtx.mensa.api.rest;


import com.github.xabgesagtx.mensa.model.Mensa;
import com.github.xabgesagtx.mensa.persistence.MensaRepository;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * Rest controller for mensas
 */
@RestController
@CrossOrigin
@RequestMapping("rest/mensa")
@Api(tags = "mensa")
@RequiredArgsConstructor
public class MensaController {

    private final MensaRepository repo;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Mensa> all() {
        return repo.findAllByOrderByName();
    }

    @GetMapping(value = "near", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mensa near(@RequestParam("longitude") Double longitude, @RequestParam("latitude") Double latitude) {
        return repo.findByPointNear(new Point(longitude, latitude), new Distance(1000, Metrics.KILOMETERS)).stream().findFirst().orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find any mensa near to you"));
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mensa findById(@PathVariable("id") String id) {
        return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No mensa with id " + id));
    }
}
