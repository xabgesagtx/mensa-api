package com.github.xabgesagtx.mensa.scraping;

import com.github.xabgesagtx.mensa.scraping.config.MensaDefaultProperties;
import com.github.xabgesagtx.mensa.scraping.model.MensaDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MensaDetailsDefaultProvider {

	private final MensaDefaultProperties properties;

	Optional<MensaDetails> get(String mensaId) {
		return Optional.ofNullable(properties.getDetails().get(mensaId));
	}
}
