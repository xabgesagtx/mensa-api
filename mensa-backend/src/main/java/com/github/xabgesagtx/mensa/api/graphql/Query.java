package com.github.xabgesagtx.mensa.api.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.github.xabgesagtx.mensa.model.Mensa;
import com.github.xabgesagtx.mensa.persistence.MensaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Root resolver for graphql query
 */
@Component
@RequiredArgsConstructor
public class Query implements GraphQLQueryResolver {

    private final MensaRepository repo;

    public Mensa mensa(String id) {
        return repo.findById(id).orElse(null);
    }

}
