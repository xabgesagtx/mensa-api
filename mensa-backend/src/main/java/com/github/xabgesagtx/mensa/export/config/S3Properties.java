package com.github.xabgesagtx.mensa.export.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties("export.s3")
public class S3Properties {
	private String key;
	private String secret;
	private String bucket;
	private String region;
	private String endpoint;
}
