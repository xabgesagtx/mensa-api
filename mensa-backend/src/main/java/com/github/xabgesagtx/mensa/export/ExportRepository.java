package com.github.xabgesagtx.mensa.export;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.github.xabgesagtx.mensa.export.config.S3Properties;
import com.github.xabgesagtx.mensa.export.model.ExportData;
import com.github.xabgesagtx.mensa.export.model.ExportException;
import com.github.xabgesagtx.mensa.export.model.ExportSummary;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
@Slf4j
@RequiredArgsConstructor
public class ExportRepository {

	private final S3Properties properties;
	private final AmazonS3 s3client;

	public List<ExportSummary> findAll() {
		List<ExportSummary> result = new ArrayList<>();
		try {
			ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(properties.getBucket());
			ListObjectsV2Result objectListing;
			do {
				objectListing = s3client.listObjectsV2(req);
				List<ExportSummary> exports = objectListing.getObjectSummaries().stream().flatMap(this::toExportDTO).collect(Collectors.toList());
				result.addAll(exports);
				String token = objectListing.getNextContinuationToken();
				req.setContinuationToken(token);
			} while (objectListing.isTruncated());
		} catch (SdkClientException e) {
			throw new ExportException("Could not load exports", e);
		}
		Collections.sort(result);
		return result;
	}

	public Optional<ExportData> getExport(LocalDateTime dateTime) {
		Optional<ExportData> result = Optional.empty();
		try {
			String dateTimeId = formatDateTimeId(dateTime);
			String filename = getFilename(dateTimeId);
			S3Object object = s3client.getObject(properties.getBucket(), filename);
			result = Optional.of(new ExportData(dateTimeId, dateTime, object.getObjectMetadata().getContentLength(), object.getObjectContent()));
		} catch (AmazonServiceException e) {
			if (e.getStatusCode() != 404) {
				throw new ExportException("Could not retrieve export", e);
			}
		} catch (SdkClientException e) {
			throw new ExportException("Could not retrieve export", e);
		}
		return result;
	}

	@NotNull
	private String getFilename(String dateTimeId) {
		return dateTimeId + ".zip";
	}

	@NotNull
	private String formatDateTimeId(LocalDateTime dateTime) {
		return ExportConstants.EXPORT_DATE_FORMATTER.format(dateTime);
	}

	public void saveExport(LocalDateTime dateTime, InputStream inputStream, long size) {
		try {
			String dateTimeId = formatDateTimeId(dateTime);
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType("application/zip");
			metadata.setContentLength(size);
			String filename = getFilename(dateTimeId);
			s3client.putObject(properties.getBucket(), filename, inputStream, metadata);
		} catch (SdkClientException e) {
			throw new ExportException("Could not save export", e);
		}
	}

	private Stream<ExportSummary> toExportDTO(S3ObjectSummary file) {
		Stream<ExportSummary> result = Stream.empty();
		String size = humanReadableByteCount(file.getSize());
		String name = file.getKey();
		String dateString = StringUtils.removeEnd(name,".zip");
		if (StringUtils.isNotBlank(dateString)) {
			try {
				LocalDateTime dateTime = LocalDateTime.parse(dateString, ExportConstants.EXPORT_DATE_FORMATTER);
				result = Stream.of(new ExportSummary(dateString, dateTime, size));
			} catch (DateTimeParseException e) {
				log.debug("Ignoring file {} due to parse error: {}", name, e.getMessage());
			}
		} else {
			log.debug("Ignoring file {}", name);
		}
		return result;
	}

	private String humanReadableByteCount(long bytes) {
		int unit = 1024;
		if (bytes < unit) return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = "KMGTPE".charAt(exp-1) + "i";
		return String.format(Locale.ENGLISH, "%.2f %sB", bytes / Math.pow(unit, exp), pre);
	}

}
