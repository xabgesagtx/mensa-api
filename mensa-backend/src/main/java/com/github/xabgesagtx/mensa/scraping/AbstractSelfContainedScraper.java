package com.github.xabgesagtx.mensa.scraping;

/**
 * Scraper that doesn't need additional information to scraping content
 * @param <T> type to be scraped
 */
public abstract class AbstractSelfContainedScraper<T> extends AbstractSimpleScraper<T> {

    public AbstractSelfContainedScraper(WebUtils utils) {
        super(utils);
    }

    /**
     * Scrape without additional information
     * @return the scraped content
     */
    public abstract T scrape();

}
