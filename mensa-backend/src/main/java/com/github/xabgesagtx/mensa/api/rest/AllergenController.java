package com.github.xabgesagtx.mensa.api.rest;

import com.github.xabgesagtx.mensa.model.Allergen;
import com.github.xabgesagtx.mensa.persistence.AllergenRepository;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * Rest controller for allergens
 */
@RestController
@CrossOrigin
@RequestMapping("rest/allergen")
@Api(tags = "allergen")
@RequiredArgsConstructor
public class AllergenController {

    private final AllergenRepository repo;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Allergen> all() {
        return repo.findAllByOrderByNumberAsc();
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Allergen byId(@PathVariable("id") String id) {
        return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No allergen with id " + id));
    }
}
