package com.github.xabgesagtx.mensa.export.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Configuration of export
 */
@Getter
@Setter
@Component
@ConfigurationProperties("export")
public class ExportProperties {

	private String tempDir;

}
