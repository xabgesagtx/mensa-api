package com.github.xabgesagtx.mensa.persistence;

import com.github.xabgesagtx.mensa.model.Dish;
import com.github.xabgesagtx.mensa.model.QDish;
import com.querydsl.core.types.dsl.StringExpression;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.time.LocalDate;
import java.util.List;

/**
 * Repository for dishes
 */
public interface DishRepository extends MongoRepository<Dish, String>, QuerydslPredicateExecutor<Dish>, QuerydslBinderCustomizer<QDish> {
	
	List<Dish> findByDateAndMensaIdOrderById(LocalDate date, String mensaId);

	List<Dish> findByMensaId(String mensaId);

	@Override
	default void customize(QuerydslBindings bindings, QDish root) {
		bindings.bind(root.description).first(StringExpression::containsIgnoreCase);
		bindings.bind(root.to).first((path, value) -> root.date.before(value.plusDays(1)));
		bindings.bind(root.from).first((path, value) -> root.date.after(value.minusDays(1)));
	}
}
