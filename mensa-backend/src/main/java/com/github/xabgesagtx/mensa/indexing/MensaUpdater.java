package com.github.xabgesagtx.mensa.indexing;

import com.github.xabgesagtx.mensa.model.Mensa;
import com.github.xabgesagtx.mensa.persistence.MensaRepository;
import com.github.xabgesagtx.mensa.scraping.MensaScraper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Scrapes all available mensas and updates the information available for them
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class MensaUpdater {
	
	private final MensaRepository repo;
	private final MensaScraper scraper;

	@Scheduled(cron = "${indexing.mensa.cron}")
	public void update() {
		log.info("Starting indexing of mensas");
		scraper.scrape().forEach(this::updateMensa);
		log.info("Finished indexing of mensas");
	}
	
	private void updateMensa(Mensa mensa) {
		log.info("Updating mensa {} ({})", mensa.getName(), mensa.getId());
		repo.save(mensa);
	}

}
