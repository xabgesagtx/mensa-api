package com.github.xabgesagtx.mensa.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Model for an allergen
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName="of")
@EqualsAndHashCode
@ToString
@Document(collection = "allergen")
public class Allergen implements Comparable<Allergen> {

	@Id
	@NonNull
	private String number;
	private String name;

	@Override
	public int compareTo(Allergen o) {
		return this.getNumber().compareTo(o.getNumber());
	}
}
