package com.github.xabgesagtx.mensa.startup;

import com.github.xabgesagtx.mensa.export.MensaExporter;
import com.github.xabgesagtx.mensa.indexing.AllMenuUpdateStarter;
import com.github.xabgesagtx.mensa.indexing.AllergenUpdater;
import com.github.xabgesagtx.mensa.indexing.LabelUpdater;
import com.github.xabgesagtx.mensa.indexing.MensaUpdater;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class StartupManager {

    private final MensaUpdater mensaUpdater;
    private final AllergenUpdater allergenUpdater;
    private final LabelUpdater labelUpdater;
    private final AllMenuUpdateStarter menuUpdateStarter;
    private final MensaExporter exporter;

    @PostConstruct
    public void onStartUp() {
        mensaUpdater.update();
        allergenUpdater.update();
        labelUpdater.update();
        menuUpdateStarter.startUpdates();
        exporter.export();
    }

}


