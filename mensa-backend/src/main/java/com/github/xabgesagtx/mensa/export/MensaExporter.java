package com.github.xabgesagtx.mensa.export;

import com.github.xabgesagtx.mensa.export.config.ExportProperties;
import com.github.xabgesagtx.mensa.export.jobs.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

/**
 * Runs periodic export jobs of mensa database
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class MensaExporter {

	private final AutowireCapableBeanFactory beanFactory;
	private final TaskExecutor taskExecutor;
	private final ExportProperties properties;

	@Scheduled(cron = "${export.cron}")
	public void export() {
	log.info("Starting export jobs");
		createTempPath().ifPresent(tempPath -> {
			startMensaExportJob(tempPath);
			startDishesExportJob(tempPath);
			startAllergensExportJob(tempPath);
			startLabelsExportJob(tempPath);
			startZipAndCopyJob(tempPath);
			startCleanUpJob(tempPath);
		});
	}

	private void startLabelsExportJob(Path tempPath) {
		ExportLabelsJob job = beanFactory.getBean(ExportLabelsJob.class, tempPath);
		taskExecutor.execute(job);
	}

	private void startAllergensExportJob(Path tempPath) {
		ExportAllergensJob job = beanFactory.getBean(ExportAllergensJob.class, tempPath);
		taskExecutor.execute(job);
	}

	private void startDishesExportJob(Path tempPath) {
		ExportDishesJob job = beanFactory.getBean(ExportDishesJob.class, tempPath);
		taskExecutor.execute(job);
	}

	private void startZipAndCopyJob(Path tempPath) {
		ZipAndCopyJob job = beanFactory.getBean(ZipAndCopyJob.class);
		job.setExportDirPath(tempPath);
		taskExecutor.execute(job);
	}

	private void startCleanUpJob(Path tempPath) {
		CleanUpJob job = beanFactory.getBean(CleanUpJob.class);
		job.setExportDirPath(tempPath);
		taskExecutor.execute(job);
	}

	private void startMensaExportJob(Path tempPath) {
		ExportMensasJob expo = beanFactory.getBean(ExportMensasJob.class, tempPath);
		taskExecutor.execute(expo);
	}

	private Optional<Path> createTempPath() {
		Optional<Path> result = Optional.empty();
		Path tempPath = Paths.get(properties.getTempDir(), Long.toString(System.currentTimeMillis()));
		if (Files.exists(tempPath)) {
			log.error("Cannot create temp directory {} because it already exists", tempPath);
		} else {
			try {
				result = Optional.of(Files.createDirectory(tempPath));
			} catch (IOException e) {
				log.error("Failed to create directory {} due to error: {}", tempPath, e.getMessage());
			}
		}
		return result;
	}
}
