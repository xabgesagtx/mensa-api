package com.github.xabgesagtx.mensa.indexing.config;

import com.github.xabgesagtx.mensa.time.TimeUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TimeUtilsConfig {

	@Bean
	public TimeUtils timeUtils() {
		return new TimeUtils();
	}

}
