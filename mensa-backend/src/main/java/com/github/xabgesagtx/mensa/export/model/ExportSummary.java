package com.github.xabgesagtx.mensa.export.model;

import lombok.Value;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

@Value
public class ExportSummary implements Comparable<ExportSummary> {
	String dateTimeId;
	LocalDateTime dateTime;
	String size;

	@Override
	public int compareTo(@NotNull ExportSummary o) {
		return new CompareToBuilder().append(o.dateTime,dateTime).toComparison();
	}
}
