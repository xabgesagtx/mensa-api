package com.github.xabgesagtx.mensa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Main application class for the backend
 */
@SpringBootApplication
@EnableScheduling
public class MensaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MensaBackendApplication.class, args);
	}

}
