package com.github.xabgesagtx.mensa.export.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_GATEWAY)
public class ExportException extends RuntimeException {

	public ExportException(String message, Throwable cause) {
		super(message, cause);
	}

}
