package com.github.xabgesagtx.mensa.export.model;

import lombok.EqualsAndHashCode;
import lombok.Value;

import java.io.InputStream;
import java.time.LocalDateTime;

@Value
@EqualsAndHashCode(exclude = "inputStream")
public class ExportData {
	String dateTimeId;
	LocalDateTime dateTime;
	long size;
	InputStream inputStream;
}
