package com.github.xabgesagtx.mensa.indexing;

import com.github.xabgesagtx.mensa.model.Allergen;
import com.github.xabgesagtx.mensa.persistence.AllergenRepository;
import com.github.xabgesagtx.mensa.scraping.AllergenScraper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Updater to trigger scraping of allergens and replacing the ones saved so far
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AllergenUpdater {

    private final AllergenScraper scraper;
    private final AllergenRepository repo;

    @Scheduled(cron = "${indexing.allergen.cron}")
    public void update() {
        log.info("Starting allergen indexing");
        List<Allergen> allergens = scraper.scrape();
        if (!allergens.isEmpty()) {
            List<Allergen> oldAllergens = repo.findAll();
            log.info("Replacing {} old allergens with {} new ones", oldAllergens.size(), allergens.size());
            repo.deleteAll(oldAllergens);
            repo.saveAll(allergens);
            log.info("Finished allergen indexing");
        } else {
            log.info("No allergens found");
        }
    }

}
