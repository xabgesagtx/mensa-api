package com.github.xabgesagtx.mensa.bot.keyboard;

import com.github.xabgesagtx.mensa.bot.BotConstants;
import com.github.xabgesagtx.mensa.bot.messages.Messages;
import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReplyKeyboardFactory {

	private final MessagesService messagesService;

	public Optional<ReplyKeyboard> create(ReplyKeyboardType type) {
		Optional<ReplyKeyboard> keyboard = Optional.empty();
		switch (type) {
			case MAIN:
				keyboard = Optional.of(createMainSelectKeyboard());
				break;
			case WEEKDAYS:
				keyboard = Optional.of(createSelectDayOfWeekKeyboard(false));
				break;
			case WEEKDAYS_WITH_SATURDAY:
				keyboard = Optional.of(createSelectDayOfWeekKeyboard(true));
				break;
			default:
				break;
		}
		return keyboard;
	}

	/**
	 * Creates the main select menu for selecting a date
	 * @return the keyboard
	 */
	private ReplyKeyboard createMainSelectKeyboard() {
		ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup();
		KeyboardRow firstRow = new KeyboardRow();
		firstRow.add(messagesService.getMessage(Messages.COMMAND_DATE_TODAY));
		firstRow.add(messagesService.getMessage(Messages.COMMAND_DATE_TOMORROW));
		KeyboardRow secondRow = new KeyboardRow();
		secondRow.add(messagesService.getMessage(Messages.COMMAND_CHANGE_MENSA));
		secondRow.add(messagesService.getMessage(Messages.COMMAND_WEEKDAYS));
		KeyboardRow thirdRow = new KeyboardRow();
		KeyboardButton selectNearestMensaButton = new KeyboardButton(messagesService.getMessage(Messages.COMMAND_SELECT_NEAREST_MENSA));
		selectNearestMensaButton.setRequestLocation(true);
		thirdRow.add(selectNearestMensaButton);
		keyboard.setResizeKeyboard(true);
		keyboard.setKeyboard(Arrays.asList(firstRow, secondRow, thirdRow));
		return keyboard;
	}

	/**
	 * Creates the select menu for selecting a day of week
	 * @return the keyboard
	 */
	private ReplyKeyboard createSelectDayOfWeekKeyboard(boolean withSaturday) {
		ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup();
		KeyboardRow firstRow = new KeyboardRow();
		firstRow.add(DayOfWeek.MONDAY.getDisplayName(TextStyle.FULL, BotConstants.DEFAULT_LOCALE));
		firstRow.add(DayOfWeek.TUESDAY.getDisplayName(TextStyle.FULL, BotConstants.DEFAULT_LOCALE));
		firstRow.add(DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.FULL, BotConstants.DEFAULT_LOCALE));
		KeyboardRow secondRow = new KeyboardRow();
		secondRow.add(DayOfWeek.THURSDAY.getDisplayName(TextStyle.FULL, BotConstants.DEFAULT_LOCALE));
		secondRow.add(DayOfWeek.FRIDAY.getDisplayName(TextStyle.FULL, BotConstants.DEFAULT_LOCALE));
		if (withSaturday) {
			secondRow.add(DayOfWeek.SATURDAY.getDisplayName(TextStyle.FULL, BotConstants.DEFAULT_LOCALE));
		}
		secondRow.add(messagesService.getMessage(Messages.COMMAND_BACK));
		keyboard.setResizeKeyboard(true);
		keyboard.setKeyboard(Arrays.asList(firstRow, secondRow));
		return keyboard;
	}


}
