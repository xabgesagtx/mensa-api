package com.github.xabgesagtx.mensa.bot;

import com.github.xabgesagtx.mensa.bot.apimethods.SendMessageFactory;
import com.github.xabgesagtx.mensa.bot.commands.DefaultResultSupplier;
import com.github.xabgesagtx.mensa.bot.commands.IMensaChatCommand;
import com.github.xabgesagtx.mensa.bot.commands.SelectMensaCommand;
import com.github.xabgesagtx.mensa.bot.commands.SelectNearestMensaCommand;
import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import com.github.xabgesagtx.mensa.mensa.MensaService;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;
import com.github.xabgesagtx.mensa.users.BotUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Location;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class ChatMessageHandler {

	private final List<IMensaChatCommand> mensaAwareChatCommands;
	private final BotUserService botUserService;
	private final MensaService mensaService;
	private final SelectMensaCommand selectMensaCommand;
	private final SelectNearestMensaCommand selectNearestMensaCommand;
	private final DefaultResultSupplier defaultResultSupplier;
	private final SendMessageFactory sendMessageFactory;

	public SendMessage handle(Message message) {
		CommandResult commandResult;
		Long chatId = message.getChatId();
		if (message.hasLocation()) {
			commandResult = handleLocation(chatId, message.getLocation());
		} else {
			String text = StringUtils.trimToEmpty(message.getText());
			commandResult = getMensa(chatId).map(mensa -> handleTextForMensa(text, mensa)).orElseGet(selectMensaCommand::createResult);
		}
		return sendMessageFactory.createFromCommandResult(commandResult, chatId);
	}


	private CommandResult handleTextForMensa(String text, MensaBotDTO mensa) {
		CommandResult commandResult;
		if (selectMensaCommand.matches(text)) {
			commandResult = selectMensaCommand.createResult();
		} else {
			commandResult = mensaAwareChatCommands.stream()
					.map(command -> command.toResponse(text, mensa))
					.filter(Optional::isPresent)
					.map(Optional::get)
					.findFirst()
					.orElseGet(defaultResultSupplier);
		}
		return commandResult;
	}

	private CommandResult handleLocation(Long chatId, Location location) {
		return selectNearestMensaCommand.createResult(chatId, location.getLongitude(), location.getLatitude());
	}

	private Optional<MensaBotDTO> getMensa(Long chatId) {
		return botUserService.find(chatId)
				.flatMap(user -> mensaService.find(user.getMensaId()));
	}

}
