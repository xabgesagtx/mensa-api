package com.github.xabgesagtx.mensa.bot.keyboard;

import com.github.xabgesagtx.mensa.bot.BotConstants;
import com.github.xabgesagtx.mensa.bot.config.TelegramProperties;
import com.github.xabgesagtx.mensa.bot.filter.FilterInfo;
import com.github.xabgesagtx.mensa.bot.messages.Messages;
import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MenuInlineButtonFactory {

	private final TelegramProperties config;
	private final MessagesService messagesService;

	public List<InlineButtonContent> create(String mensaId, LocalDate date, String selectedValue) {

		return config.getFilters().stream().map(filter-> {
			InlineButtonContent buttonContent;
			if (StringUtils.equals(selectedValue, filter.getValue())) {
				String label = messagesService.getMessage(Messages.FILTER_ALL);
				String value = FilterInfo.of(BotConstants.FILTER_ALL_VALUE, mensaId, date).toString();
				buttonContent = new InlineButtonContent(label, value);
			} else {
				String label = messagesService.getMessage(filter.getMessageKey());
				String value = FilterInfo.of(filter.getValue(), mensaId, date).toString();
				buttonContent = new InlineButtonContent(label, value);
			}
			return buttonContent;
		}).collect(Collectors.toList());
	}
}
