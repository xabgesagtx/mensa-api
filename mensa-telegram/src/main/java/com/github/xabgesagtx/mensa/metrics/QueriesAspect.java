package com.github.xabgesagtx.mensa.metrics;

import com.github.xabgesagtx.mensa.bot.model.FilterQueryResult;
import com.github.xabgesagtx.mensa.bot.queries.FilterMenuQuery;
import com.github.xabgesagtx.mensa.bot.queries.SelectMensaQuery;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

import java.util.Optional;

@Aspect
@Component
@RequiredArgsConstructor
public class QueriesAspect {

	private final MensaMetrics metrics;

	@Before(value = "execution(* com.github.xabgesagtx.mensa.bot.CallbackQueryHandler.handle(..)) && args(query)", argNames = "query")
	public void queryIncoming(CallbackQuery query) {
		metrics.incQueriesIncoming();
		if (query.getMessage() != null) {
			metrics.incQueriesIncomingChat(query.getMessage().getChatId());
		}
	}

	@AfterReturning(value = "execution(* com.github.xabgesagtx.mensa.bot.queries.FilterMenuQuery.toResult(..))", returning = "result")
	public void handleFilterQuery(Optional<FilterQueryResult> result) {
		result.ifPresent(ignored -> metrics.incQueriesHandled(FilterMenuQuery.class));
	}

	@AfterReturning(value = "execution(* com.github.xabgesagtx.mensa.bot.queries.SelectMensaQuery.toResult(..))", returning = "result")
	public void handleSelectMensaQuery(Optional<String> result) {
		result.ifPresent(ignored -> metrics.incQueriesHandled(SelectMensaQuery.class));
	}

}
