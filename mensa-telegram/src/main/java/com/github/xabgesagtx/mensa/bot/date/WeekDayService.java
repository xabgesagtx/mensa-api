package com.github.xabgesagtx.mensa.bot.date;

import com.github.xabgesagtx.mensa.bot.BotConstants;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class WeekDayService {

	private final TodaySupplier todaySupplier;

	public Optional<LocalDate> getDate(String dayOfWeekText) {
		return getDayOfWeekFromString(dayOfWeekText)
				.map(dayOfWeek -> getDateForDayOfWeek(dayOfWeek, todaySupplier.get()));
	}

	private LocalDate getDateForDayOfWeek(DayOfWeek dayOfWeek, LocalDate date) {
		LocalDate result;
		DayOfWeek currentDayOfWeek = date.getDayOfWeek();
		int diff = dayOfWeek.getValue() - currentDayOfWeek.getValue();
		if (diff >= 0) {
			result = date.plusDays(diff);
		} else {
			result = date.plusWeeks(1).plusDays(diff);
		}
		return result;
	}

	private Optional<DayOfWeek> getDayOfWeekFromString(String text) {
		return Arrays.stream(DayOfWeek.values())
				.filter(dayOfWeek -> matchesDayOfWeek(dayOfWeek, text))
				.findFirst();
	}

	private boolean matchesDayOfWeek(DayOfWeek dayOfWeek, String text) {
		return Stream.of(TextStyle.FULL,TextStyle.SHORT).anyMatch(style -> StringUtils.equalsIgnoreCase(dayOfWeek.getDisplayName(style, BotConstants.DEFAULT_LOCALE),text));
	}
}
