package com.github.xabgesagtx.mensa.mensa.model;

import lombok.Data;

@Data
public class MensaApiDTO {

	private String id;
	private String name;

}
