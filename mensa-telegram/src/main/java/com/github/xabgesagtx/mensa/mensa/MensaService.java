package com.github.xabgesagtx.mensa.mensa;

import com.github.xabgesagtx.mensa.mensa.model.MensaApiDTO;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MensaService {

	private final MensaApiClient client;

	public List<MensaBotDTO> findAllMensas() {
		return client.findAllMensas().stream().map(this::toDto).collect(Collectors.toList());
	}

	public Optional<MensaBotDTO> findNearestMensa(double longitude, double latitude) {
		return client.findNearestMensa(longitude, latitude).map(this::toDto);
	}

	public Optional<MensaBotDTO> find(String mensaId) {
		return client.find(mensaId).map(this::toDto);
	}

	private MensaBotDTO toDto(MensaApiDTO mensa) {
		return MensaBotDTO.of(mensa.getId(), mensa.getName());
	}
}
