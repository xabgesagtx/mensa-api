package com.github.xabgesagtx.mensa.bot.commands;

import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardType;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardType;
import com.github.xabgesagtx.mensa.bot.messages.Messages;
import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;
import com.github.xabgesagtx.mensa.time.TimeUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@RequiredArgsConstructor
public class WeekDayKeyboardCommand implements IMatchingMensaChatCommand {

	private final MessagesService messagesService;
	private final TimeUtils timeUtils;

	@Override
	public boolean matches(String text) {
		return messagesService.getMessage(Messages.COMMAND_WEEKDAYS).equalsIgnoreCase(text);
	}

	@Override
	public CommandResult createResult(String text, MensaBotDTO mensa) {
		String messageText = messagesService.getMessage(Messages.RESPONSE_SELECT_WEEKDAY);
		ReplyKeyboardType replyKeyboardType = timeUtils.isMensaOpenOnSaturday(mensa.getId()) ? ReplyKeyboardType.WEEKDAYS_WITH_SATURDAY : ReplyKeyboardType.WEEKDAYS;
		return new CommandResult(messageText, replyKeyboardType, InlineKeyboardType.NONE, Collections.emptyList());
	}
}
