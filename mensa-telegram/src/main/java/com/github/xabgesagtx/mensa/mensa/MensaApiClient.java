package com.github.xabgesagtx.mensa.mensa;

import com.github.xabgesagtx.mensa.common.MensaApiException;
import com.github.xabgesagtx.mensa.common.MensaApiProperties;
import com.github.xabgesagtx.mensa.mensa.model.MensaApiDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
class MensaApiClient {

	private static final String MENSAS_PATH = "/rest/mensa";
	private static final String MENSA_BY_ID_PATH_TEMPLATE = "/rest/mensa/{id}";
	private static final String NEAREST_MENSA_PATH = "/rest/mensa/near";

	private final RestTemplate restTemplate;
	private final MensaApiProperties properties;

	List<MensaApiDTO> findAllMensas() {
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(MENSAS_PATH)
				.toUriString();
		try {
			return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<MensaApiDTO>>() {}).getBody();
		} catch (RestClientException e) {
			throw new MensaApiException("Could not load mensas", e);
		}
	}

	Optional<MensaApiDTO> find(String id) {
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(MENSA_BY_ID_PATH_TEMPLATE)
				.buildAndExpand(id)
				.toUriString();
		return retrieve(uri);
	}

	Optional<MensaApiDTO> findNearestMensa(double longitude, double latitude) {
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(NEAREST_MENSA_PATH)
				.queryParam("longitude", longitude)
				.queryParam("latitude", latitude)
				.toUriString();
		return retrieve(uri);

	}

	private Optional<MensaApiDTO> retrieve(String uri) {
		try {
			MensaApiDTO body = restTemplate.getForObject(uri, MensaApiDTO.class);
			return Optional.ofNullable(body);
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
				return Optional.empty();
			} else {
				throw new MensaApiException("Could not retrieve mensa", e);
			}
		} catch (RestClientException e) {
			throw new MensaApiException("Could not retrieve mensa", e);
		}
	}

}
