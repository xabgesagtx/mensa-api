package com.github.xabgesagtx.mensa.bot.config;

import com.github.xabgesagtx.mensa.bot.commands.RelativeDateCommand;
import com.github.xabgesagtx.mensa.bot.date.TodaySupplier;
import com.github.xabgesagtx.mensa.bot.date.TomorrowSupplier;
import com.github.xabgesagtx.mensa.bot.keyboard.MenuInlineButtonFactory;
import com.github.xabgesagtx.mensa.bot.messages.Messages;
import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import com.github.xabgesagtx.mensa.bot.templates.DishMessageTemplate;
import com.github.xabgesagtx.mensa.dishes.DishService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommandConfig {

	@Bean
	public RelativeDateCommand todayCommand(DishService dishService, DishMessageTemplate template, MenuInlineButtonFactory inlineButtonFactory, TodaySupplier todaySupplier, MessagesService messagesService) {
		String todayCommandMessage = messagesService.getMessage(Messages.COMMAND_DATE_TODAY);
		return new RelativeDateCommand(dishService, template, inlineButtonFactory, todaySupplier, todayCommandMessage);
	}

	@Bean
	public RelativeDateCommand tomorrowCommand(DishService dishService, DishMessageTemplate template, MenuInlineButtonFactory inlineButtonFactory, TomorrowSupplier tomorrowSupplier, MessagesService messagesService) {
		String todayCommandMessage = messagesService.getMessage(Messages.COMMAND_DATE_TOMORROW);
		return new RelativeDateCommand(dishService, template, inlineButtonFactory, tomorrowSupplier, todayCommandMessage);
	}
}
