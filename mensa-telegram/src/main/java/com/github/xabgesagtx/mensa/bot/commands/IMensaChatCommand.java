package com.github.xabgesagtx.mensa.bot.commands;

import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;

import java.util.Optional;

public interface IMensaChatCommand extends IChatCommand {

	Optional<CommandResult> toResponse(String text, MensaBotDTO mensa);
}
