package com.github.xabgesagtx.mensa.metrics;

import com.github.xabgesagtx.mensa.bot.commands.IChatCommand;
import com.github.xabgesagtx.mensa.bot.queries.ICallbackQuery;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@RequiredArgsConstructor
public class MensaMetrics {

	private static final String METRIC_MESSAGES_HANDLED = "messages.handled";
	private static final String METRIC_QUERIES_HANDLED = "queries.handled";
	private static final String METRIC_MESSAGES_INCOMING = "messages.incoming";
	private static final String METRIC_MESSAGES_INCOMING_CHAT = "messages.incoming.chat";
	private static final String METRIC_QUERIES_INCOMING = "queries.incoming";
	private static final String METRIC_QUERIES_INCOMING_CHAT = "queries.incoming.chat";

	private static final String TAG_COMMAND = "command";
	private static final String TAG_CHAT_ID = "chatId";


	private final MeterRegistry registry;
	private final List<IChatCommand> chatCommands;
	private final List<ICallbackQuery> callbackQueries;

	void incMessagesHandled(Class<?> clazz) {
		messagesHandled(clazz).increment();
	}

	void incQueriesHandled(Class<?> clazz) {
		queriesHandled(clazz).increment();
	}

	void incMessagesIncoming() {
		messagesIncoming().increment();
	}

	void incMessagesIncomingChat(long chatId) {
		registry.counter(METRIC_MESSAGES_INCOMING_CHAT, TAG_CHAT_ID, String.valueOf(chatId)).increment();
	}

	void incQueriesIncoming() {
		queriesIncoming().increment();
	}

	void incQueriesIncomingChat(long chatId) {
		registry.counter(METRIC_QUERIES_INCOMING_CHAT, TAG_CHAT_ID, String.valueOf(chatId)).increment();
	}

	private Counter queriesIncoming() {
		return registry.counter(METRIC_QUERIES_INCOMING);
	}


	private Counter messagesHandled(Class<?> clazz) {
		return registry.counter(METRIC_MESSAGES_HANDLED, TAG_COMMAND, clazz.getSimpleName());
	}

	private Counter messagesIncoming() {
		return registry.counter(METRIC_MESSAGES_INCOMING);
	}

	private Counter queriesHandled(Class<?> clazz) {
		return registry.counter(METRIC_QUERIES_HANDLED, TAG_COMMAND, clazz.getSimpleName());
	}

	@PostConstruct
	public void initCounters() {
		messagesIncoming();
		queriesIncoming();
		chatCommands.stream()
				.map(ClassUtils::getUserClass)
				.forEach(this::messagesHandled);
		callbackQueries.stream()
				.map(ClassUtils::getUserClass)
				.forEach(this::queriesHandled);
	}


}
