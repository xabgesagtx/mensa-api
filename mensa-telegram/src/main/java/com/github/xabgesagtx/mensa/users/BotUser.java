package com.github.xabgesagtx.mensa.users;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Class to hold information of a bot user
 */
@Getter
@Setter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Document(collection = "botuser")
class BotUser {

    @Id
    private Long chatId;
    private String mensaId;

}
