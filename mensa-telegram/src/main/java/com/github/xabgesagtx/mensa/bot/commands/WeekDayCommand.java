package com.github.xabgesagtx.mensa.bot.commands;

import com.github.xabgesagtx.mensa.bot.BotConstants;
import com.github.xabgesagtx.mensa.bot.date.WeekDayService;
import com.github.xabgesagtx.mensa.bot.keyboard.InlineButtonContent;
import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardType;
import com.github.xabgesagtx.mensa.bot.keyboard.MenuInlineButtonFactory;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardType;
import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import com.github.xabgesagtx.mensa.bot.templates.DishMessageTemplate;
import com.github.xabgesagtx.mensa.dishes.DishService;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class WeekDayCommand implements IMensaChatCommand {

	private final DishService dishService;
	private final DishMessageTemplate template;
	private final MenuInlineButtonFactory inlineButtonFactory;
	private final WeekDayService weekDayService;


	@Override
	public Optional<CommandResult> toResponse(String text, MensaBotDTO mensa) {
		return weekDayService.getDate(text)
				.map(date -> dishService.getDishesForNextOpeningDay(date, mensa))
				.map(dishRetrievalResult -> {
					List<InlineButtonContent> inlineButtons = inlineButtonFactory.create(mensa.getId(), dishRetrievalResult.getDate(), BotConstants.FILTER_ALL_VALUE);
					String messageText = template.writeAsString(mensa, dishRetrievalResult);
					return new CommandResult(messageText, ReplyKeyboardType.MAIN, InlineKeyboardType.FILTER, inlineButtons);
				});
	}
}
