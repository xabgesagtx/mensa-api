package com.github.xabgesagtx.mensa.bot.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Main configuration class for the bot
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "telegram")
public class TelegramProperties {

    private String botname;
    private String token;
    private List<FilterProperties> filters;

}
