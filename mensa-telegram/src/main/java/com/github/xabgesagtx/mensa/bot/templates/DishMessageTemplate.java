package com.github.xabgesagtx.mensa.bot.templates;

import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import com.github.xabgesagtx.mensa.dishes.DishRetrievalResult;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class DishMessageTemplate {

	private static final DateTimeFormatter TEMPLATE_DATE_FORMAT = DateTimeFormatter.ofPattern("EEEE, dd.MM.yyyy", Locale.GERMAN);

	private final Configuration freemarkerConfig;
	private final MessagesService messagesService;

	public String writeAsString(MensaBotDTO mensa, DishRetrievalResult dishRetrievalResult) {
		String result;
		try {
			Map<String,Object> root = new HashMap<>();
			root.put("mensa", mensa);
			root.put("date", TEMPLATE_DATE_FORMAT.format(dishRetrievalResult.getDate()));
			if (dishRetrievalResult.getOriginalDate() != null) {
				root.put("originalDate", TEMPLATE_DATE_FORMAT.format(dishRetrievalResult.getOriginalDate()));
			}
			root.put("dishes", dishRetrievalResult.getDishes());
			root.put("messagesService", messagesService);
			Template template = freemarkerConfig.getTemplate("dishes.ftl");
			StringWriter out = new StringWriter();
			template.process(root, out);
			result = out.toString();
		} catch (IOException e) {
			log.error("Failed to load template: {}", e.getMessage());
			result = "Internal Server Error :)";
		} catch (TemplateException e) {
			log.error("Failed to process template for {} and day {}: {}", mensa.getId(), dishRetrievalResult.getDate(), e.getMessage());
			result = "Internal Server Error :)";
		}
		return result;
	}

}
