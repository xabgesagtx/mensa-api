package com.github.xabgesagtx.mensa.bot.commands;

import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;

import java.util.Optional;

public interface IMatchingMensaChatCommand extends IMensaChatCommand {

	boolean matches(String text);

	CommandResult createResult(String text, MensaBotDTO mensa);

	default Optional<CommandResult> toResponse(String text, MensaBotDTO mensa) {
		if (matches(text)) {
			return Optional.of(createResult(text, mensa));
		} else {
			return Optional.empty();
		}
	}
}
