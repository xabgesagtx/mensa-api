package com.github.xabgesagtx.mensa.dishes;

import com.github.xabgesagtx.mensa.dishes.model.DishApiDTO;
import com.github.xabgesagtx.mensa.dishes.model.DishBotDTO;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;
import com.github.xabgesagtx.mensa.time.TimeUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DishService {

	private final TimeUtils timeUtils;
	private final DishApiClient client;

	public List<DishBotDTO> findByDate(LocalDate date, String mensaId) {
		return convert(client.find(mensaId, date));
	}

	public List<DishBotDTO> findByDateWithoutLabels(String mensaId, LocalDate date, List<String> labelsToFilter) {
		return client.find(mensaId, date).stream()
				.filter(dish -> labelsToFilter.stream().noneMatch(label -> dish.getLabels().contains(label)))
				.map(this::toDto)
				.collect(Collectors.toList());
	}

	public List<DishBotDTO> findByDateWithLabels(String mensaId, LocalDate date, List<String> labelsToFilter) {
		return client.find(mensaId, date).stream()
				.filter(dish -> labelsToFilter.stream().anyMatch(label -> dish.getLabels().contains(label)))
				.map(this::toDto)
				.collect(Collectors.toList());
	}

	public DishRetrievalResult getDishesForNextOpeningDay(LocalDate date, MensaBotDTO mensa) {
		LocalDate dateToUse;
		LocalDate originalDate;
		if (!timeUtils.isOpeningDay(date, mensa.getId())) {
			dateToUse = timeUtils.nextOpeningDay(date, mensa.getId());
			originalDate = date;
		} else {
			dateToUse = date;
			originalDate = null;
		}
		List<DishBotDTO> dishes = findByDate(dateToUse, mensa.getId());
		return new DishRetrievalResult(dateToUse, originalDate, dishes);
	}

	private List<DishBotDTO> convert(List<DishApiDTO> dishes) {
		return dishes.stream().map(this::toDto).collect(Collectors.toList());
	}

	private DishBotDTO toDto(DishApiDTO dish) {
		String prices = dish.getPrices().stream().map(price -> String.format("%,.2f €", price)).collect(Collectors.joining(" / "));
		return new DishBotDTO(dish.getCategory(), dish.getDescription(), dish.getLabels(), prices);
	}
}
