package com.github.xabgesagtx.mensa.bot.keyboard;

public enum ReplyKeyboardType {

	NONE, MAIN, WEEKDAYS, WEEKDAYS_WITH_SATURDAY

}
