package com.github.xabgesagtx.mensa.bot.queries;

import com.github.xabgesagtx.mensa.bot.messages.Messages;
import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
@RequiredArgsConstructor
public class DefaultMessageSupplier implements Supplier<String> {

	private final MessagesService messagesService;

	@Override
	public String get() {
		return messagesService.getMessage(Messages.RESPONSE_DONT_UNDERSTAND);
	}
}
