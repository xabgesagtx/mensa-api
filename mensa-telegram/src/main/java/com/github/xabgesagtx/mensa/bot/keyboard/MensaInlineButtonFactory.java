package com.github.xabgesagtx.mensa.bot.keyboard;

import com.github.xabgesagtx.mensa.mensa.MensaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MensaInlineButtonFactory {

	private final MensaService mensaService;

	public List<InlineButtonContent> create() {
		return mensaService.findAllMensas().stream()
				.map(mensa -> new InlineButtonContent(mensa.getName(), mensa.getId()))
				.collect(Collectors.toList());
	}

}
