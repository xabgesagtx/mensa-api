package com.github.xabgesagtx.mensa.bot.apimethods;

import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardFactory;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardFactory;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardType;
import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Service
@RequiredArgsConstructor
public class SendMessageFactory {

	private final InlineKeyboardFactory inlineKeyboardFactory;
	private final ReplyKeyboardFactory replyKeyboardFactory;

	public SendMessage createFromCommandResult(CommandResult commandResult, Long chatId) {
		SendMessage message = new SendMessage();
		message.setChatId(chatId);
		message.setText(commandResult.getText());
		message.setParseMode("HTML");
		replyKeyboardFactory.create(commandResult.getReplyKeyboardType()).ifPresent(message::setReplyMarkup);
		inlineKeyboardFactory.create(commandResult.getInlineKeyboardType(), commandResult.getInlineButtons()).ifPresent(message::setReplyMarkup);
		return message;
	}

	public SendMessage createFromText(String text, ReplyKeyboardType replyKeyboardType, Long chatId) {
		SendMessage message = new SendMessage();
		message.setChatId(chatId);
		message.setText(text);
		message.setParseMode("HTML");
		replyKeyboardFactory.create(replyKeyboardType).ifPresent(message::setReplyMarkup);
		return message;
	}

}
