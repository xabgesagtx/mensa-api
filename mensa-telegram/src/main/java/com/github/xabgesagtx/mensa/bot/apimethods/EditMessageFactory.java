package com.github.xabgesagtx.mensa.bot.apimethods;

import com.github.xabgesagtx.mensa.bot.keyboard.InlineButtonContent;
import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardFactory;
import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EditMessageFactory {

	private final InlineKeyboardFactory inlineKeyboardFactory;

	public EditMessageText create(Long chatId, Integer messageId, String text, List<InlineButtonContent> inlineButtons) {
		EditMessageText message = new EditMessageText();
		message.setChatId(chatId);
		message.setMessageId(messageId);
		message.setText(text);
		message.setParseMode("HTML");
		inlineKeyboardFactory.create(InlineKeyboardType.FILTER, inlineButtons).ifPresent(message::setReplyMarkup);
		return message;
	}

}
