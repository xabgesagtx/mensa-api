package com.github.xabgesagtx.mensa.bot.commands;

import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardType;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardType;
import com.github.xabgesagtx.mensa.bot.messages.Messages;
import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import com.github.xabgesagtx.mensa.mensa.MensaService;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;
import com.github.xabgesagtx.mensa.users.BotUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@RequiredArgsConstructor
public class SelectNearestMensaCommand implements IChatCommand {

	private final MensaService mensaService;
	private final BotUserService botUserService;
	private final MessagesService messagesService;

	public CommandResult createResult(Long chatId, double longitude, double latitude) {
		return mensaService.findNearestMensa(longitude, latitude)
				.map(mensa -> {
					botUserService.saveMensaForUser(chatId, mensa.getId());
					return nearestMensaSaved(mensa);
				})
				.orElseGet(this::noNearestMensa);
	}

	private CommandResult nearestMensaSaved(MensaBotDTO mensa) {
		String message = messagesService.getMessage(Messages.RESPONSE_SELECT_DAY, mensa.getName());
		return new CommandResult(message, ReplyKeyboardType.MAIN, InlineKeyboardType.NONE, Collections.emptyList());
	}

	private CommandResult noNearestMensa() {
		String message = messagesService.getMessage(Messages.RESPONSE_NO_NEAREST_MENSA);
		return new CommandResult(message, ReplyKeyboardType.NONE, InlineKeyboardType.NONE, Collections.emptyList());
	}
}
