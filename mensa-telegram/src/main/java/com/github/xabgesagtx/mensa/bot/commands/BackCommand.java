package com.github.xabgesagtx.mensa.bot.commands;

import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardType;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardType;
import com.github.xabgesagtx.mensa.bot.messages.Messages;
import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@RequiredArgsConstructor
public class BackCommand implements IMatchingMensaChatCommand {

	private final MessagesService messagesService;

	@Override
	public boolean matches(String text) {
		return messagesService.getMessage(Messages.COMMAND_BACK).equalsIgnoreCase(text);
	}

	@Override
	public CommandResult createResult(String text, MensaBotDTO mensa) {
		String messageText = messagesService.getMessage(Messages.RESPONSE_SELECT_DAY, mensa.getName());
		return new CommandResult(messageText, ReplyKeyboardType.MAIN, InlineKeyboardType.NONE, Collections.emptyList());
	}
}
