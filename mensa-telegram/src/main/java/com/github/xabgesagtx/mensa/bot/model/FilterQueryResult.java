package com.github.xabgesagtx.mensa.bot.model;

import com.github.xabgesagtx.mensa.bot.keyboard.InlineButtonContent;
import lombok.Value;

import java.util.List;

@Value
public class FilterQueryResult {

	String text;
	List<InlineButtonContent> inlineButtons;

}
