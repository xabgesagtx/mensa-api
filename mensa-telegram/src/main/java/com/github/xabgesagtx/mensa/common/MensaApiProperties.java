package com.github.xabgesagtx.mensa.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties("mensa-api")
public class MensaApiProperties {
	private String url;
}
