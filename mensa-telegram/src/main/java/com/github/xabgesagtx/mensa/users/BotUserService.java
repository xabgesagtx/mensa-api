package com.github.xabgesagtx.mensa.users;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BotUserService {

	private final BotUserRepository repo;

	public Optional<BotUserDTO> find(long chatId) {
		return repo.findByChatId(chatId)
				.map(botUser -> BotUserDTO.of(botUser.getChatId(), botUser.getMensaId()));
	}

	public void saveMensaForUser(Long chatId, String mensaId) {
		BotUser botUser = repo.findByChatId(chatId).map(user -> {
			user.setMensaId(mensaId);
			return user;
		}).orElseGet(() -> BotUser.of(chatId, mensaId));
		repo.save(botUser);
	}

}
