package com.github.xabgesagtx.mensa.bot.keyboard;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class InlineKeyboardFactory {


	public Optional<InlineKeyboardMarkup> create(InlineKeyboardType type, List<InlineButtonContent> inlineButtons) {
		Optional<InlineKeyboardMarkup> result;
		switch (type) {
			case MENSAS:
				result = Optional.of(createSingleColumn(inlineButtons));
				break;
			case FILTER:
				result = Optional.of(createDoubleColumn(inlineButtons));
				break;
			case NONE:
			default:
				result = Optional.empty();
				break;
		}
		return result;

	}

	private InlineKeyboardMarkup createSingleColumn(List<InlineButtonContent> inlineButtons) {
		List<List<InlineKeyboardButton>> rows = inlineButtons.stream()
				.map(this::createButton)
				.map(Collections::singletonList)
				.collect(Collectors.toList());
		InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
		inlineKeyboardMarkup.setKeyboard(rows);
		return inlineKeyboardMarkup;



	}

	private InlineKeyboardMarkup createDoubleColumn(List<InlineButtonContent> inlineButtons) {
		AtomicInteger counter = new AtomicInteger();
		List<List<InlineKeyboardButton>> rows = inlineButtons.stream()
				.map(this::createButton)
				.collect(Collectors.groupingBy(it -> counter.getAndIncrement() / 2))
				.entrySet()
				.stream()
				.sorted(Comparator.comparingInt(Map.Entry::getKey))
				.map(Map.Entry::getValue)
				.collect(Collectors.toList());
		InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
		inlineKeyboardMarkup.setKeyboard(rows);
		return inlineKeyboardMarkup;
	}


	private InlineKeyboardButton createButton(InlineButtonContent content) {
		InlineKeyboardButton button = new InlineKeyboardButton();
		button.setText(content.getLabel());
		button.setCallbackData(content.getValue());
		return button;
	}

}
