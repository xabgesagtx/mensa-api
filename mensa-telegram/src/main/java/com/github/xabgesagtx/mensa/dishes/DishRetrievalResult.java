package com.github.xabgesagtx.mensa.dishes;

import com.github.xabgesagtx.mensa.dishes.model.DishBotDTO;
import lombok.Value;

import java.time.LocalDate;
import java.util.List;

@Value
public class DishRetrievalResult {
	LocalDate date;
	LocalDate originalDate;
	List<DishBotDTO> dishes;
}
