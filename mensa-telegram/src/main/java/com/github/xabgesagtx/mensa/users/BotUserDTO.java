package com.github.xabgesagtx.mensa.users;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(staticName = "of")
public class BotUserDTO {

	long chatId;
	String mensaId;
}
