package com.github.xabgesagtx.mensa.bot.commands;

import com.github.xabgesagtx.mensa.bot.BotConstants;
import com.github.xabgesagtx.mensa.bot.keyboard.InlineButtonContent;
import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardType;
import com.github.xabgesagtx.mensa.bot.keyboard.MensaInlineButtonFactory;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardType;
import com.github.xabgesagtx.mensa.bot.messages.Messages;
import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SelectMensaCommand implements IChatCommand {

	private final MessagesService messagesService;
	private final MensaInlineButtonFactory inlineButtonFactory;

	public boolean matches(String text) {
		return StringUtils.equalsIgnoreCase(BotConstants.START_COMMAND, text) || StringUtils.equalsIgnoreCase(messagesService.getMessage(Messages.COMMAND_CHANGE_MENSA), text);
	}

	public CommandResult createResult() {
		String text = messagesService.getMessage(Messages.RESPONSE_SELECT_MENSA);
		List<InlineButtonContent> inlineButtons = inlineButtonFactory.create();
		return new CommandResult(text, ReplyKeyboardType.NONE, InlineKeyboardType.MENSAS, inlineButtons);
	}
}
