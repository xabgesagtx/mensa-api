package com.github.xabgesagtx.mensa.bot.queries;

import com.github.xabgesagtx.mensa.bot.messages.Messages;
import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import com.github.xabgesagtx.mensa.mensa.MensaService;
import com.github.xabgesagtx.mensa.users.BotUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class SelectMensaQuery implements ICallbackQuery {

	private final MensaService mensaService;
	private final BotUserService botUserService;
	private final MessagesService messagesService;

	public Optional<String> toResult(Long chatId, String text) {
		return mensaService.find(text).map(mensa -> {
			botUserService.saveMensaForUser(chatId, mensa.getId());
			return messagesService.getMessage(Messages.RESPONSE_SELECT_DAY, mensa.getName());
		});
	}

}
