package com.github.xabgesagtx.mensa.bot.model;

import com.github.xabgesagtx.mensa.bot.keyboard.InlineButtonContent;
import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardType;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardType;
import lombok.Value;

import java.util.List;

@Value
public class CommandResult {

	String text;
	ReplyKeyboardType replyKeyboardType;
	InlineKeyboardType inlineKeyboardType;
	List<InlineButtonContent> inlineButtons;

}
