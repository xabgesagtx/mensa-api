package com.github.xabgesagtx.mensa.dishes.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * DTO to be used for displaying the dish in a bot response
 */
@Getter
@AllArgsConstructor
public class DishBotDTO {

    private final String category;
    private final String description;
    private final List<String> labels;
    private final String prices;

}
