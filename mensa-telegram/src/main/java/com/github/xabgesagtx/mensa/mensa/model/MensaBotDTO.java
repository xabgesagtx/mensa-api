package com.github.xabgesagtx.mensa.mensa.model;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(staticName = "of")
public class MensaBotDTO {

	String id;
	String name;

}
