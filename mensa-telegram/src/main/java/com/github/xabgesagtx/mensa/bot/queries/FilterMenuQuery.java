package com.github.xabgesagtx.mensa.bot.queries;

import com.github.xabgesagtx.mensa.bot.config.FilterProperties;
import com.github.xabgesagtx.mensa.bot.config.TelegramProperties;
import com.github.xabgesagtx.mensa.bot.filter.FilterInfo;
import com.github.xabgesagtx.mensa.bot.filter.FilterType;
import com.github.xabgesagtx.mensa.bot.keyboard.InlineButtonContent;
import com.github.xabgesagtx.mensa.bot.keyboard.MenuInlineButtonFactory;
import com.github.xabgesagtx.mensa.bot.model.FilterQueryResult;
import com.github.xabgesagtx.mensa.bot.templates.DishMessageTemplate;
import com.github.xabgesagtx.mensa.dishes.DishRetrievalResult;
import com.github.xabgesagtx.mensa.dishes.DishService;
import com.github.xabgesagtx.mensa.dishes.model.DishBotDTO;
import com.github.xabgesagtx.mensa.mensa.MensaService;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FilterMenuQuery implements ICallbackQuery {

	private final MensaService mensaService;
	private final DishService dishService;
	private final MenuInlineButtonFactory menuInlineButtonFactory;
	private final DishMessageTemplate template;
	private final TelegramProperties config;

	public Optional<FilterQueryResult> toResult(String text) {
		return FilterInfo.fromString(text).flatMap(filterInfo ->
			mensaService.find(filterInfo.getMensaId()).map(mensa ->
				createFilterQueryResult(filterInfo, mensa)
			)
		);
	}

	private FilterQueryResult createFilterQueryResult(FilterInfo filterInfo, MensaBotDTO mensa) {
		List<DishBotDTO> dishes = getDishBotDTOs(filterInfo);
		DishRetrievalResult dishRetrievalResult = new DishRetrievalResult(filterInfo.getDate(), null, dishes);
		String messageText = template.writeAsString(mensa, dishRetrievalResult);
		List<InlineButtonContent> inlineButtons = menuInlineButtonFactory.create(mensa.getId(), filterInfo.getDate(), filterInfo.getValue());
		return new FilterQueryResult(messageText, inlineButtons);
	}

	private List<DishBotDTO> getDishBotDTOs(FilterInfo filterInfo) {
		Optional<FilterProperties> filterConfigOpt = config.getFilters().stream().filter(filter -> StringUtils.equals(filter.getValue(), filterInfo.getValue())).findFirst();
		if (filterConfigOpt.isPresent()) {
			FilterProperties filterConfig = filterConfigOpt.get();
			if (filterConfig.getType() == FilterType.NEGATIVE) {
				return dishService.findByDateWithoutLabels(filterInfo.getMensaId(), filterInfo.getDate(), filterConfig.getLabelsToFilter());
			} else {
				return dishService.findByDateWithLabels(filterInfo.getMensaId(), filterInfo.getDate(), filterConfig.getLabelsToFilter());
			}
		} else {
			return dishService.findByDate(filterInfo.getDate(), filterInfo.getMensaId());
		}
	}
}
