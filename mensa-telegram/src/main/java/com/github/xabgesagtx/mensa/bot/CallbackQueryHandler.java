package com.github.xabgesagtx.mensa.bot;

import com.github.xabgesagtx.mensa.bot.apimethods.EditMessageFactory;
import com.github.xabgesagtx.mensa.bot.apimethods.SendMessageFactory;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardType;
import com.github.xabgesagtx.mensa.bot.queries.DefaultMessageSupplier;
import com.github.xabgesagtx.mensa.bot.queries.FilterMenuQuery;
import com.github.xabgesagtx.mensa.bot.queries.SelectMensaQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CallbackQueryHandler {

	private final FilterMenuQuery filterMenuQuery;
	private final SelectMensaQuery selectMensaQuery;
	private final EditMessageFactory editMessageFactory;
	private final SendMessageFactory sendMessageFactory;
	private final DefaultMessageSupplier defaultMessageSupplier;

	public List<BotApiMethod> handle(CallbackQuery query) {
		return handleCallbackQuery(query);
	}

	private List<BotApiMethod> handleCallbackQuery(CallbackQuery query) {
		String data = query.getData();
		String queryId = query.getId();
		AnswerCallbackQuery simpleCallbackAnswer = simpleCallbackAnswer(queryId);
		if (query.getMessage() == null) {
			log.info("Ignoring callback query because message is empty");
			return Collections.singletonList(simpleCallbackAnswer);
		} else {
			Long chatId = query.getMessage().getChatId();
			Integer messageId = query.getMessage().getMessageId();
			BotApiMethod response = filterMenuQuery.toResult(data)
					.map(filterQueryResult -> (BotApiMethod)editMessageFactory.create(chatId, messageId, filterQueryResult.getText(), filterQueryResult.getInlineButtons()))
					.orElseGet(() -> {
						String text = selectMensaQuery.toResult(chatId, data).orElseGet(defaultMessageSupplier);
						return sendMessageFactory.createFromText(text, ReplyKeyboardType.MAIN, chatId);
					});
			return Arrays.asList(simpleCallbackAnswer, response);
		}
	}

	private AnswerCallbackQuery simpleCallbackAnswer(String queryId) {
		AnswerCallbackQuery answer = new AnswerCallbackQuery();
		answer.setCallbackQueryId(queryId);
		return answer;
	}

}
