package com.github.xabgesagtx.mensa.metrics;

import com.github.xabgesagtx.mensa.bot.commands.SelectMensaCommand;
import com.github.xabgesagtx.mensa.bot.commands.SelectNearestMensaCommand;
import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.Optional;

@Aspect
@Component
@RequiredArgsConstructor
public class ChatCommandsAspect {

	private final MensaMetrics metrics;

	@Before(value = "execution(* com.github.xabgesagtx.mensa.bot.ChatMessageHandler.handle(..)) && args(message)", argNames = "message")
	public void chatMessageIncoming(Message message) {
		metrics.incMessagesIncoming();
		metrics.incMessagesIncomingChat(message.getChatId());
	}

	@AfterReturning(value = "execution(* com.github.xabgesagtx.mensa.bot.commands.IMensaChatCommand.toResponse(..))", returning = "result")
	public void handleChatMessageHandled(JoinPoint jp, Optional<CommandResult> result) {
		result.ifPresent(ignored -> {
			Class<?> clazz = jp.getTarget().getClass();
			metrics.incMessagesHandled(clazz);
		});
	}

	@AfterReturning(value = "execution(* com.github.xabgesagtx.mensa.bot.commands.SelectNearestMensaCommand.createResult(..))")
	public void handleSelectNearestMensaCommand() {
		metrics.incMessagesHandled(SelectNearestMensaCommand.class);
	}

	@AfterReturning(value = "execution(* com.github.xabgesagtx.mensa.bot.commands.SelectMensaCommand.createResult(..))")
	public void handleSelectMensaCommand() {
		metrics.incMessagesHandled(SelectMensaCommand.class);
	}

}
