package com.github.xabgesagtx.mensa.bot;

import com.github.xabgesagtx.mensa.bot.config.TelegramProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

/**
 * Mensa bot to access mensa content
 */
@Component
@Slf4j
@RequiredArgsConstructor
class MensaBot extends TelegramLongPollingBot {

    private final ChatMessageHandler chatMessageHandler;
    private final CallbackQueryHandler callbackQueryHandler;
    private final TelegramProperties config;

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            handleMessage(update.getMessage());
        } else if (update.hasCallbackQuery()) {
            handleCallbackQuery(update.getCallbackQuery());
        } else {
            log.info("Unsupported indexing: {}", update.toString());
        }
    }

    private void handleCallbackQuery(CallbackQuery query) {
        callbackQueryHandler.handle(query).forEach(this::sendApiMethodSafe);
    }


    private void handleMessage(Message message) {
        SendMessage response = chatMessageHandler.handle(message);
        sendResponse(message.getChatId(), response);
    }

    private void sendApiMethodSafe(BotApiMethod<?> method) {
        try {
            sendApiMethod(method);
        } catch (TelegramApiException e) {
            log.error("Failed to send response due to error: {}", e.getMessage());
        }
    }

    private void sendResponse(Long chatId, SendMessage result) {
        try {
            log.info("Sending response to {}", chatId);
            result.setChatId(chatId);
            result.setParseMode("HTML");
            sendApiMethod(result);
        } catch (TelegramApiException e) {
            log.error("Failed to send response to {} due to error: {}", chatId, e.getMessage());
        }
    }

    @Override
    public String getBotUsername() {
        return config.getBotname();
    }

    @Override
    public String getBotToken() {
        return config.getToken();
    }
}
