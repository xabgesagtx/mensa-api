package com.github.xabgesagtx.mensa.dishes;

import com.github.xabgesagtx.mensa.common.MensaApiException;
import com.github.xabgesagtx.mensa.common.MensaApiProperties;
import com.github.xabgesagtx.mensa.dishes.model.DishApiDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
@RequiredArgsConstructor
class DishApiClient {

	private static final String DISHES_PATH = "/rest/dish";

	private final RestTemplate restTemplate;
	private final MensaApiProperties properties;

	List<DishApiDTO> find(String mensaId, LocalDate date) {
		String formattedDate = DateTimeFormatter.ISO_DATE.format(date);
		String uri = UriComponentsBuilder.fromHttpUrl(properties.getUrl())
				.path(DISHES_PATH)
				.queryParam("mensaId", mensaId)
				.queryParam("date", formattedDate)
				.toUriString();
		try {
			return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<DishApiDTO>>() {}).getBody();
		} catch (RestClientException e) {
			throw new MensaApiException("Could not retrieve dishes", e);
		}
	}

}
