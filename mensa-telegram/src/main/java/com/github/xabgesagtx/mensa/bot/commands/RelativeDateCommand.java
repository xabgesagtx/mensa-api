package com.github.xabgesagtx.mensa.bot.commands;

import com.github.xabgesagtx.mensa.bot.BotConstants;
import com.github.xabgesagtx.mensa.bot.keyboard.InlineButtonContent;
import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardType;
import com.github.xabgesagtx.mensa.bot.keyboard.MenuInlineButtonFactory;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardType;
import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import com.github.xabgesagtx.mensa.bot.templates.DishMessageTemplate;
import com.github.xabgesagtx.mensa.dishes.DishRetrievalResult;
import com.github.xabgesagtx.mensa.dishes.DishService;
import com.github.xabgesagtx.mensa.mensa.model.MensaBotDTO;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Supplier;

@RequiredArgsConstructor
public class RelativeDateCommand implements IMatchingMensaChatCommand {

	private final DishService dishService;
	private final DishMessageTemplate template;
	private final MenuInlineButtonFactory inlineButtonFactory;
	private final Supplier<LocalDate> dateSupplier;
	private final String dateMessageText;

	@Override
	public boolean matches(String text) {
		return text.equalsIgnoreCase(dateMessageText);
	}

	@Override
	public CommandResult createResult(String text, MensaBotDTO mensa) {
		LocalDate today = dateSupplier.get();
		DishRetrievalResult dishRetrievalResult = dishService.getDishesForNextOpeningDay(today, mensa);
		List<InlineButtonContent> inlineButtons = inlineButtonFactory.create(mensa.getId(), dishRetrievalResult.getDate(), BotConstants.FILTER_ALL_VALUE);
		String message = template.writeAsString(mensa, dishRetrievalResult);
		return new CommandResult(message, ReplyKeyboardType.MAIN, InlineKeyboardType.FILTER, inlineButtons);
	}
}
