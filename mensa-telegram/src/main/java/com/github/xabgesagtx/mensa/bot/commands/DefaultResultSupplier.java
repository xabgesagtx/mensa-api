package com.github.xabgesagtx.mensa.bot.commands;

import com.github.xabgesagtx.mensa.bot.keyboard.InlineKeyboardType;
import com.github.xabgesagtx.mensa.bot.keyboard.ReplyKeyboardType;
import com.github.xabgesagtx.mensa.bot.messages.Messages;
import com.github.xabgesagtx.mensa.bot.messages.MessagesService;
import com.github.xabgesagtx.mensa.bot.model.CommandResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.function.Supplier;

@Component
@RequiredArgsConstructor
public class DefaultResultSupplier implements Supplier<CommandResult> {

	private final MessagesService messagesService;

	@Override
	public CommandResult get() {
		String text = messagesService.getMessage(Messages.RESPONSE_DONT_UNDERSTAND);
		return new CommandResult(text, ReplyKeyboardType.NONE, InlineKeyboardType.NONE, Collections.emptyList());
	}
}
