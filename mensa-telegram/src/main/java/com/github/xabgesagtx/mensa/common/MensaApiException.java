package com.github.xabgesagtx.mensa.common;

public class MensaApiException extends RuntimeException {

	public MensaApiException(String message, Throwable cause) {
		super(message, cause);
	}
}
