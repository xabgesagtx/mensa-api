package com.github.xabgesagtx.mensa.bot.keyboard;

import lombok.Value;

@Value
public class InlineButtonContent {

	private String label;
	private String value;

}
