package com.github.xabgesagtx.mensa.dishes.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class DishApiDTO {

	private final String category;
	private final String description;
	private final List<String> labels;
	private final List<BigDecimal> prices;

}
